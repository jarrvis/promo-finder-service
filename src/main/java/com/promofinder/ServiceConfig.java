package com.promofinder;


import com.promofinder.security.ApiKeyFilter;
import com.promofinder.security.ApiKeyHelper;
import com.promofinder.util.spring.QueryStringResolver;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.web.servlet.FilterRegistrationBean;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.context.support.PropertySourcesPlaceholderConfigurer;
import org.springframework.context.support.ResourceBundleMessageSource;
import org.springframework.web.method.support.HandlerMethodArgumentResolver;
import org.springframework.web.servlet.LocaleResolver;
import org.springframework.web.servlet.config.annotation.WebMvcConfigurerAdapter;
import org.springframework.web.servlet.i18n.SessionLocaleResolver;
import java.util.List;
import java.util.Locale;

/**
 * Main configuration class of the TrackMyTools eShop API. This class configures various aspects of Spring WebMVC
 * framework.
 * 
 * @author kai8wz
 *
 */
@Configuration
public class ServiceConfig extends WebMvcConfigurerAdapter {

    private static final Logger LOG = LoggerFactory.getLogger(ServiceConfig.class);

    @Bean
    public static PropertySourcesPlaceholderConfigurer propertyConfigIn() {
        return new PropertySourcesPlaceholderConfigurer();
    }

    @Bean
    public ResourceBundleMessageSource messageSource() {
        final ResourceBundleMessageSource messageSource = new ResourceBundleMessageSource();
        messageSource.setBasename("lang/messages");
        return messageSource;
    }

    @Bean
    public LocaleResolver localeResolver(){
        SessionLocaleResolver sessionLocaleResolver = new SessionLocaleResolver();
        sessionLocaleResolver.setDefaultLocale(Locale.ENGLISH);
        return sessionLocaleResolver;
    }

    @Override
    public void addArgumentResolvers(List<HandlerMethodArgumentResolver> argumentResolvers) {
        final QueryStringResolver queryStringResolver = new QueryStringResolver();
        argumentResolvers.add(queryStringResolver);
    }

    @Autowired
    ApiKeyHelper apiKeyHelperComponent;

    /**
     * The FilterRegistrationBean. This will instruct Spring Boot to do the
     * filter before every request.
     *
     * @return
     */
    @Bean
    public FilterRegistrationBean filterRegistrationBean() {
        final FilterRegistrationBean registrationBean =
                new FilterRegistrationBean();
        final ApiKeyFilter securityFilter = new ApiKeyFilter(
                apiKeyHelperComponent);
        registrationBean.setFilter(securityFilter);
        registrationBean.setOrder(Integer.MAX_VALUE);
        return registrationBean;
    }

    /**
     * The ApiKeyHelperBean.
     *
     * @return
     */
    @Bean
    public ApiKeyHelper helperApiKeyBean() {
        return new ApiKeyHelper();
    }


}
