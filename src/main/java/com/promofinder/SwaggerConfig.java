package com.promofinder;


import org.springframework.beans.factory.annotation.Value;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import springfox.documentation.builders.ApiInfoBuilder;
import springfox.documentation.builders.PathSelectors;
import springfox.documentation.builders.RequestHandlerSelectors;
import springfox.documentation.service.ApiInfo;
import springfox.documentation.spi.DocumentationType;
import springfox.documentation.spring.web.plugins.Docket;
import springfox.documentation.swagger2.annotations.EnableSwagger2;


/**
 * Main configuration class to enable the Swagger UI frontend. It registers all supported controllers and describes the
 * API.
 * 
 * @author kai8wz
 * 
 */

@Configuration
@EnableSwagger2
public class SwaggerConfig {

    // Swagger UI
    public static final String SWAGGER_UI_ENDPOINT = "/swagger-ui.html";
    public static final String SWAGGER_RESOURCES_ENDPOINT = "/swagger-resources";
    public static final String SWAGGER_API_DOCS_ENDPOINT = "/v2/api-docs";

    @Value("${info.build.version}")
    private String apiVersion;
    @Value("${info.build.name}")
    private String apiTitle;
    @Value("${info.build.artifact}")
    private String apiGroupName;

    @Bean
    public Docket api() {
        return new Docket(DocumentationType.SWAGGER_2)
                .select()
                .apis(RequestHandlerSelectors.any())
                .paths(PathSelectors.any())
                .build();
    }

    private ApiInfo apiInfo() {
        return new ApiInfoBuilder()
                .title(apiTitle)
                .description(
                        "The eShop Service provides access to the internal RB.Payment and RB.Subscription services "
                                + "and is the interface to the Web-Shop running in the browser.").termsOfServiceUrl("")
                .contact("Lars Rottmann <fixed-term.lars.rottmann@de.bosch.com>").version(apiVersion).build();
    }

}
