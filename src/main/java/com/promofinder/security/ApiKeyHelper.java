package com.promofinder.security;


import com.promofinder.ApiConfiguration;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

/**
 * The api key helper that validates apikeys.
 * 
 * @author Benjamin Fischer (Benjamin.Fischer@de.bosch.com)
 *
 */
@Component
public class ApiKeyHelper {

    public static final String APIKEY_HEADER = "apikey";

    @Autowired
    ApiConfiguration apiConfig;

    private static final Logger LOG = LoggerFactory
            .getLogger(ApiKeyHelper.class);

    /**
     * Try to validate the given apikey against the known ones.
     * 
     * @param apiKeyToValidate
     *            The apikey that should be validated.
     * @return The usage of the apikey or null if the apikey is unknown.
     */
    public String validateApiKey(String apiKeyToValidate) {

        LOG.debug("Starting validation of API Key: {}", apiKeyToValidate);

        if (apiConfig == null) {
            LOG.error("The api config cannot be loaded. No apikeys are available");
            return null;
        }

        if (apiConfig.getSecurity() == null) {
            LOG.error("The security node of the config cannot be loaded.");
            return null;
        }

        if (apiConfig.getSecurity().getAuthenticatedApikeys() == null) {
            LOG.error("No apikeys are available in the config file of the service.");
            return null;
        }

        for (final ApiConfiguration.ApiKey apikey : apiConfig.getSecurity()
                .getAuthenticatedApikeys()) {

            if (apikey.getApikey().equals(apiKeyToValidate)) {
                return apikey.getName();
            }
        }

        return null;
    }
}
