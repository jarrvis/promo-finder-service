package com.promofinder.security;


import com.fasterxml.jackson.databind.ObjectMapper;
import com.fasterxml.jackson.databind.ObjectWriter;
import com.promofinder.api.exception.UnauthorizedException;
import com.promofinder.model.ErrorResource;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.security.authentication.UsernamePasswordAuthenticationToken;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.web.filter.OncePerRequestFilter;

import javax.servlet.FilterChain;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;
import java.util.Date;

/**
 * Filters all incoming requests. This filter will retrieve the API key from the
 * header and looks for a corresponding user. If no @User is found, a 405 is
 * returned.
 * 
 * @author FBS8FE
 *
 */
public class ApiKeyFilter extends OncePerRequestFilter {

    private static final String ERROR_UNAUTHORIZED_REQUEST_MESSAGE =
            "Unauthorized request. User does not exist. Given API Key was {}";

    private static final Logger LOG = LoggerFactory
            .getLogger(ApiKeyFilter.class);

    ApiKeyHelper apiKeyHelper;

    /**
     * Public constructor to create a new instance of the ApiKeyFilter with the
     * necessary helper.
     * 
     * @param apiKeyHelper2
     *            The helper that is needed for the construction
     */
    public ApiKeyFilter(ApiKeyHelper apiKeyHelper2) {
        apiKeyHelper = apiKeyHelper2;
    }

    @Override
    protected void doFilterInternal(HttpServletRequest request,
                                    HttpServletResponse response, FilterChain filterChain)
            throws ServletException, IOException {

        // Exclude the swagger stuff and spring actuator stuff.
        // If this is the case the filter chain is just continued
        final String path = request.getRequestURI();
        LOG.debug("Starting pre request filter");

        if (!path.contains("/import") && !path.contains("/location")) {
            filterChain.doFilter(request, response);
            return;
        }

        final String authenticatedApp = doAuthenticate(request, response);
        if (authenticatedApp == null) return;

        final UsernamePasswordAuthenticationToken authRequest =
                new UsernamePasswordAuthenticationToken(
                        authenticatedApp, null);

        SecurityContextHolder.getContext().setAuthentication(authRequest);

        LOG.debug("Finsihed pre request filter.");

        filterChain.doFilter(request, response);

    }

    private String doAuthenticate(HttpServletRequest request, HttpServletResponse response) throws IOException {
        final String apiKey = request.getHeader(ApiKeyHelper.APIKEY_HEADER);

        final String authenticatedApp = apiKeyHelper.validateApiKey(apiKey);

        try {
            if (authenticatedApp == null || authenticatedApp.isEmpty()) {

                LOG.info(
                        ERROR_UNAUTHORIZED_REQUEST_MESSAGE,
                        apiKey);

                final UnauthorizedException ue =
                        new UnauthorizedException(
                                String.format(
                                        ERROR_UNAUTHORIZED_REQUEST_MESSAGE,
                                        apiKey));
                final ErrorResource er = new ErrorResource(
                        ue.getCode(), ue
                                .getCode().value(), new Date().getTime(),
                        ue.getMessageGerman(),
                        ue.getMessageEnglish(), "");

                final ObjectWriter ow = new ObjectMapper().writer()
                        .withDefaultPrettyPrinter();
                final String jsonClientResponse = ow.writeValueAsString(er);

                response.resetBuffer();
                response.setStatus(HttpServletResponse.SC_UNAUTHORIZED);
                response.setHeader("Content-Type", "application/json");

                response.getWriter().write(jsonClientResponse);
                response.flushBuffer();
                return null;

            }
        } catch (final Exception e) {

            response.sendError(HttpServletResponse.SC_INTERNAL_SERVER_ERROR,
                    "The REST Security Server experienced an internal error.");
            return null;
        }
        return authenticatedApp;
    }
}
