package com.promofinder.api;

import com.promofinder.api.exception.InvalidRequestException;
import com.promofinder.model.Location;
import com.promofinder.model.LocationData;
import com.promofinder.service.LocationService;
import com.promofinder.service.repository.LocationRepository;
import io.swagger.annotations.*;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.PageRequest;
import org.springframework.http.MediaType;
import org.springframework.validation.BindingResult;
import org.springframework.validation.annotation.Validated;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.util.UriComponentsBuilder;

import java.util.List;
import java.util.Locale;
import java.util.Optional;


/**
 * Created by KAI8WZ on 11.06.2017.
 */

@RestController
@RequestMapping(value = "/location")
@Api(description = "REST API for operations on locations")
public class LocationController extends AbstractBaseController{

    private static final Logger LOG = LoggerFactory.getLogger(LocationController.class);

    @Autowired
    private LocationRepository locationRepository;

    @Autowired
    private LocationService locationService;


    /**
     * /create  --> Create a new user and save it in the database.
     *
     * @param location location object
     * @return created location's object
     */
    @RequestMapping(value = "/create", method = RequestMethod.POST, produces = MediaType.APPLICATION_JSON_VALUE)
    @ApiOperation(value = "Creates a location object", notes = "Creates a <b>location</b> object "
            +  "Saves it into DB.", response = Location.class)
    @ApiResponses(value = { @ApiResponse(code = 200, message = "Saved location object"),
            @ApiResponse(code = 422, message = "In case of validation errors of location object")})
    @ResponseBody
    public Location create(
    //        @ApiParam(value = "The country of the location that should be created in app", required = true) @PathVariable String country,
       //     @ApiParam(value = "The language  of the location that should be created in app", required = true) @PathVariable String language,
            @ApiParam(value = "Api key", required = true) @RequestHeader( value = "apikey", required = true) String apiKey,
            @ApiParam(value = "Location object", required = true) @RequestBody final @Validated Location location,
            BindingResult bindingResult, UriComponentsBuilder b) {

        LOG.info("Location creation request received: {}", location);

        if (location == null || bindingResult.hasErrors())
            throw new InvalidRequestException(String.format("Invalid location creation request, form data contains %s error(s).",
                    bindingResult.getErrorCount()), bindingResult, new Locale("en", "GB"));

        locationRepository.save(location);
        return location;
    }

    /**
     * /delete  --> Delete a locations object from DB.
     *
     * @param id location object id
     * @return deleted location  object
     */
    @RequestMapping(value = "/delete", method = RequestMethod.DELETE, produces = MediaType.APPLICATION_JSON_VALUE)
    @ApiOperation(value = "Deletes a location object", notes = "Deletes a <b>location</b> object "
            +  "Saves it into DB.", response = Location.class)
    @ApiResponses(value = { @ApiResponse(code = 200, message = "Saved location object"),
            @ApiResponse(code = 404, message = "In case of no location object was found in DB for given id")})
    @ResponseBody
    public Location delete(
            @ApiParam(value = "Api key", required = true) @RequestHeader( value = "apikey", required = true) String apiKey,
            @ApiParam(value = "id") @RequestParam(value = "id", required = true)  final String id) {

        LOG.info("Location deletion request received: id = " + id);
        return locationService.delete(id);
    }


    /**
     * /get-by-name  --> Return the id for the user having the passed email.
     *
     * @return The user id or a message error if the user is not found.
     */
    @RequestMapping(value = "/get-locations", method = RequestMethod.GET, produces = MediaType.APPLICATION_JSON_VALUE)
    @ApiOperation(value = "Fetch locations by name", notes = "Searches for all <b>location</b> objects in DB "
            +  "Returns list of location objects for given name.", response = LocationData.class, responseContainer = "List")
    @ApiResponses(value = { @ApiResponse(code = 200, message = "Saved location object"),
            @ApiResponse(code = 404, message = "In case of no location object was found in DB for given name")})
    @ResponseBody
    public List<LocationData> getLocations(
            @ApiParam(value = "Api key", required = true) @RequestHeader( value = "apikey", required = true) String apiKey,
            @ApiParam(value = "Limit the results list") @RequestParam(value = "limit", required = false) final String limit,
            @ApiParam(value = "Field by which results should be sorted") @RequestParam(value = "sortBy", required = false) final String sortBy) {

        LOG.info("Location get-locations request received");
        Optional<PageRequest> orderAndSort = getPageRequestOptional(limit, sortBy, LocationData.class);
        return orderAndSort.map(pageRequest -> locationService.findAll(pageRequest))
                .orElseGet(() -> locationService.findAll());
    }

    /**
     * /get-by-name  --> Return the id for the user having the passed email.
     *
     * @param name The name to search in the database.
     * @return The user id or a message error if the user is not found.
     */
    @RequestMapping(value = "/get-by-name", method = RequestMethod.GET, produces = MediaType.APPLICATION_JSON_VALUE)
    @ApiOperation(value = "Fetch locations by name", notes = "Searches for all <b>location</b> objects in DB "
            +  "Returns list of location objects for given name.")
    @ApiResponses(value = { @ApiResponse(code = 200, message = "Saved location object"),
            @ApiResponse(code = 404, message = "In case of no location object was found in DB for given name")})
    @ResponseBody
    public LocationData getByName(
            @ApiParam(value = "Api key", required = true) @RequestHeader( value = "apikey", required = true) String apiKey,
            @ApiParam(value = "name", required = true) @RequestParam(value = "name", required = true)  final String name) {

        LOG.info("Location get-by-name request received: name = " + name);
        LocationData locationData = locationService.findByName(name);
        return locationData;
    }

    /**
     * /get-by-name  --> Return the id for the user having the passed email.
     *
     * @param name The name to search in the database.
     * @return The user id or a message error if the user is not found.
     */
    @RequestMapping(value = "/update-image", method = RequestMethod.POST, produces = MediaType.APPLICATION_JSON_VALUE)
    @ApiOperation(value = "Update location data with image url", notes = "Searches for all <b>location</b> objects in DB "
            +  "Returns list of location objects for given name.")
    @ApiResponses(value = { @ApiResponse(code = 200, message = "Saved location object"),
            @ApiResponse(code = 404, message = "In case of no location object was found in DB for given name")})
    @ResponseBody
    public void updateWithImageUrl(
            @ApiParam(value = "Api key", required = true) @RequestHeader( value = "apikey", required = true) String apiKey,
            @ApiParam(value = "name", required = true) @RequestParam(value = "name", required = true)  final String name,
            @ApiParam(value = "imageUrl", required = true) @RequestParam(value = "imageUrl", required = true)  final String imageUrl){

        LOG.info("Location update-image request received: name = " + name);
        locationService.updateWithImageUrl(name, imageUrl);
    }

    /**
     * Retrieve a list of closest locations.
     *
     * @return list of Location objects
     */
    @RequestMapping(value = "/get-closest", method = RequestMethod.GET, produces = MediaType.APPLICATION_JSON_VALUE)
    @ApiOperation(value = "Returns closest locations", notes = "Lists closest <b>locations</b> that are stored in DB. "
            + "Parameters <b>howMany</b> and <b>areaRadius</b> must be passed to query for fetching locations.  "
            + "Here both are optional. By default endpoint will return 5 closest location in area of 3km radius"
            +  "In case no locations found that match search criteria, empty list is returned.\n  Optional parameters "
            +  "for sorting and pagination can be used. Default sort option is location distance.", response = Location.class, responseContainer = "List")
    @ApiResponses(value = { @ApiResponse(code = 200, message = "An array of events"),
            @ApiResponse(code = 406, message = "In case of validation errors on the sortBy or limit parameters")})
    @ResponseBody
    public List<Location> getClosestLocations(
            @ApiParam(value = "Api key", required = true) @RequestHeader( value = "apikey", required = true) String apiKey,
            @ApiParam(value = "User longitude", required = true) @RequestParam(value = "longitude", required = true)  final String longitude,
            @ApiParam(value = "User latitude", required = true) @RequestParam(value = "latitude", required = true)  final String latitude,
            @ApiParam(value = "How many locations should be fetched from DB") @RequestParam(defaultValue  = "5", required = false) String howMany,
            @ApiParam(value = "Radius of area in which searching should be done") @RequestParam(defaultValue  = "3", required = false) String areaRadius,
            @ApiParam(value = "Limit the results list") @RequestParam(value = "limit", required = false) final String limit,
            @ApiParam(value = "Field by which results should be sorted") @RequestParam(value = "sortBy", required = false) final String sortBy){

        Optional<PageRequest> orderAndSort = getPageRequestOptional(limit, sortBy, Location.class);
        return orderAndSort.map(pageRequest -> locationService.findClosest(latitude, longitude, areaRadius, howMany, pageRequest))
                .orElseGet(() -> locationService.findClosest(latitude, longitude, areaRadius, howMany));
    }

}