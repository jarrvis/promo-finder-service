package com.promofinder.api;

import com.promofinder.model.LocationData;
import com.promofinder.service.FeedService;
import com.promofinder.service.LocationService;
import com.promofinder.service.remote.PyGeoLocatorImpl;
import io.swagger.annotations.*;
import org.apache.commons.lang3.StringUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.core.env.Environment;
import org.springframework.http.MediaType;
import org.springframework.web.bind.annotation.*;
import java.util.*;

/**
 * Created by KAI8WZ on 12.06.2017.
 */

@RestController
@RequestMapping(value = "/import")
@Api(description = "REST API for import operations")
public class ImporterController extends AbstractBaseController{

    private static final Logger LOG = LoggerFactory.getLogger(ImporterController.class);

    public static final String FEED_FILES_URL = "promofinder.data.feed.url";

    @Autowired
    private Environment env;

    @Autowired
    private FeedService feedService;

    @Autowired
    private LocationService locationService;

    @Autowired
    private PyGeoLocatorImpl pyGeoLocator;

    /**
     * /import  --> Import the locations by name. It will look for feed file with that name and import its content to database
     *
     * @param name The new name.
     */
    @RequestMapping(value = "/by-name", method = RequestMethod.GET, produces = MediaType.APPLICATION_JSON_VALUE)
    @ApiOperation(value = "Import locations by name", notes = "Searches for <b>location</b> file and imports content into DB "
            +  "Returns import status.")
    @ApiResponses(value = {
            @ApiResponse(code = 200, message = "Import successful"),
            @ApiResponse(code = 405, message = "In case of feed file can not be open"),
            @ApiResponse(code = 428, message = "In case of feed file path configuration problem")})
    @ResponseBody
    public void importByName(
            @ApiParam(value = "Api key", required = true) @RequestHeader( value = "apikey", required = true) String apiKey,
            @ApiParam(value = "name", required = true) @RequestParam(value = "name", required = true)  final String name) {

        String feedUrl = env.getProperty(FEED_FILES_URL);
        if(StringUtils.isBlank(feedUrl))
            throw new IllegalStateException("Data feed path not configured");

        this.feedService.readFeedFile(feedUrl, name).ifPresent(list -> {
                LocationData locationData = new LocationData(list, name);
                locationService.saveOrUpdate(locationData);
            });

    }

    /**
     * /import  --> Import all locations files. It will look for feed files import  content to database
     *
     */
    @RequestMapping(value = "/all", method = RequestMethod.GET, produces = MediaType.APPLICATION_JSON_VALUE)
    @ApiOperation(value = "Import all location feed files", notes = "Searches for all <b>location</b> files and imports content into DB "
            +  "Returns import status.")
    @ApiResponses(value = {
            @ApiResponse(code = 200, message = "Import successful"),
            @ApiResponse(code = 405, message = "In case of feed file can not be open"),
            @ApiResponse(code = 428, message = "In case of feed file path configuration problem")})
    @ResponseBody
    public void importAll(
            @ApiParam(value = "Api key", required = true) @RequestHeader( value = "apikey", required = true) String apiKey){

        String feedUrl = env.getProperty(FEED_FILES_URL);
        if(StringUtils.isBlank(feedUrl))
            throw new IllegalStateException("Data feed path not configured");

        this.feedService.readFeedFiles(feedUrl).ifPresent(list ->
                        this.locationService.saveAll(list));

    }

    /**
     * /available-for-import  --> Check what files are in feed path
     * @return A string describing if the entries were successfully added to database.
     */
    @RequestMapping(value = "/available-for-import", method = RequestMethod.GET, produces = MediaType.APPLICATION_JSON_VALUE)
    @ApiOperation(value = "Fetch location files available for import", notes = "Searches for all <b>location</b> files on import ftp "
            +  "Returns available for import location file names.")
    @ApiResponses(value = {
            @ApiResponse(code = 200, message = "Available for import location file names"),
            @ApiResponse(code = 404, message = "No feed files found"),
            @ApiResponse(code = 428, message = "In case of feed file path configuration problem")})
    @ResponseBody
    public List<String> showLocationsAvailableForImport() {

        String feedUrl = env.getProperty(FEED_FILES_URL);
        if(StringUtils.isBlank(feedUrl))
            throw new IllegalStateException("Data feed path not configured");

        return feedService.findFeedFiles(feedUrl);
    }

    /**
     * /generateFeed  --> Call
     *
     * @param name The new name.
     */
    @RequestMapping(value = "/generate-feed", method = RequestMethod.GET, produces = MediaType.APPLICATION_JSON_VALUE)
    @ApiOperation(value = "Generate feed file for importer", notes = "Generates <b>feed</b> file using pyGeoLocator")
    @ApiResponses(value = {
            @ApiResponse(code = 200, message = "Import successful"),
            @ApiResponse(code = 405, message = "In case of feed file can not be open"),
            @ApiResponse(code = 428, message = "In case of feed file path configuration problem")})
    @ResponseBody
    public void generateFeed(
            @ApiParam(value = "Api key", required = true) @RequestHeader( value = "apikey", required = true) String apiKey,
            @ApiParam(value = "name", required = true) @RequestParam(value = "name", required = true)  final String name,
            @ApiParam(value = "type", required = true) @RequestParam(value = "type", required = true)  final String type) {


            this.pyGeoLocator.generateFeed(name, type);

    }
}
