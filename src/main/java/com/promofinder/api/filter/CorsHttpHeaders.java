package com.promofinder.api.filter;

//rot8le: backport of Spring Framework 4.2, remove when Spring Boot is updated to release 1.3.0 

public final class CorsHttpHeaders {

    /**
     * The CORS {@code Access-Control-Allow-Credentials} response header field name.
     * 
     * @see <a href="http://www.w3.org/TR/cors/">CORS W3C recommandation</a>
     */
    public static final String ACCESS_CONTROL_ALLOW_CREDENTIALS = "Access-Control-Allow-Credentials";
    /**
     * The CORS {@code Access-Control-Allow-Headers} response header field name.
     * 
     * @see <a href="http://www.w3.org/TR/cors/">CORS W3C recommandation</a>
     */
    public static final String ACCESS_CONTROL_ALLOW_HEADERS = "Access-Control-Allow-Headers";
    /**
     * The CORS {@code Access-Control-Allow-Methods} response header field name.
     * 
     * @see <a href="http://www.w3.org/TR/cors/">CORS W3C recommandation</a>
     */
    public static final String ACCESS_CONTROL_ALLOW_METHODS = "Access-Control-Allow-Methods";
    /**
     * The CORS {@code Access-Control-Allow-Origin} response header field name.
     * 
     * @see <a href="http://www.w3.org/TR/cors/">CORS W3C recommandation</a>
     */
    public static final String ACCESS_CONTROL_ALLOW_ORIGIN = "Access-Control-Allow-Origin";
    /**
     * The CORS {@code Access-Control-Expose-Headers} response header field name.
     * 
     * @see <a href="http://www.w3.org/TR/cors/">CORS W3C recommandation</a>
     */
    public static final String ACCESS_CONTROL_EXPOSE_HEADERS = "Access-Control-Expose-Headers";
    /**
     * The CORS {@code Access-Control-Max-Age} response header field name.
     * 
     * @see <a href="http://www.w3.org/TR/cors/">CORS W3C recommandation</a>
     */
    public static final String ACCESS_CONTROL_MAX_AGE = "Access-Control-Max-Age";
    /**
     * The CORS {@code Access-Control-Request-Headers} request header field name.
     * 
     * @see <a href="http://www.w3.org/TR/cors/">CORS W3C recommandation</a>
     */
    public static final String ACCESS_CONTROL_REQUEST_HEADERS = "Access-Control-Request-Headers";
    /**
     * The CORS {@code Access-Control-Request-Method} request header field name.
     * 
     * @see <a href="http://www.w3.org/TR/cors/">CORS W3C recommandation</a>
     */
    public static final String ACCESS_CONTROL_REQUEST_METHOD = "Access-Control-Request-Method";

    private CorsHttpHeaders() {

    }
}
