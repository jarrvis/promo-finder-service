package com.promofinder.api.filter;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;
import org.springframework.web.filter.OncePerRequestFilter;

import javax.servlet.FilterChain;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;

/*
 * This filter will add Cross-Origin Resource Sharing (CORS) access control headers to its response
 * to allow clients to make AJAX calls to this service.
 *
 * Should be replaced with @CrossOrigin annotations when upgrading to Spring Boot 1.3.0 (Spring 4.2.0)
 */

@Component
public class CorsFilter extends OncePerRequestFilter {

    private static final Logger LOG = LoggerFactory.getLogger(CorsFilter.class);

    private final CorsProcessor processor = new DefaultCorsProcessor();

    @Autowired
    CorsConfiguration corsConfiguration;

    @Override
    protected void doFilterInternal(HttpServletRequest request, HttpServletResponse response, FilterChain filterChain)
            throws ServletException, IOException {
        if (CorsUtils.isCorsRequest(request) && corsConfiguration != null) {
            final boolean isValid = this.processor.processRequest(corsConfiguration, request, response);
            if (!isValid || CorsUtils.isPreFlightRequest(request)) {
                LOG.debug("CorsFilter is ending request processing.");
                return;
            }
        }

        LOG.debug("CorsFilter is passing request down the filter chain");

        filterChain.doFilter(request, response);
    }

}
