/*
 * Copyright 2002-2015 the original author or authors.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 * http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

// rot8le: backport of Spring Framework 4.2, remove when Spring Boot is updated to release 1.3.0 

package com.promofinder.api.filter;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpMethod;
import org.springframework.http.HttpStatus;
import org.springframework.util.CollectionUtils;
import org.springframework.util.StringUtils;
import org.springframework.web.util.UriComponents;
import org.springframework.web.util.UriComponentsBuilder;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;
import java.net.URI;
import java.net.URISyntaxException;
import java.util.ArrayList;
import java.util.Collections;
import java.util.Iterator;
import java.util.List;

/**
 * Default implementation of {@link CorsProcessor}, as defined by the <a href="http://www.w3.org/TR/cors/">CORS W3C
 * recommendation</a>.
 *
 * <p>
 * Note that when input {@link CorsConfiguration} is {@code null}, this implementation does not reject simple or actual
 * requests outright but simply avoid adding CORS headers to the response. CORS processing is also skipped if the
 * response already contains CORS headers, or if the request is detected as a same-origin one.
 *
 * @author Sebastien Deleuze
 * @author Rossen Stoyanhcev
 * @since 4.2
 */
public class DefaultCorsProcessor implements CorsProcessor {
    private static final Logger LOGGER = LoggerFactory.getLogger(DefaultCorsProcessor.class);

    @Override
    public boolean processRequest(CorsConfiguration config, HttpServletRequest request, HttpServletResponse response)
            throws IOException {
        if (!CorsUtils.isCorsRequest(request)) {
            return true;
        }
        if (isSameOrigin(request)) {
            LOGGER.debug("Skip CORS processing, request is a same-origin one");
            return true;
        }
        if (responseHasCors(response)) {
            LOGGER.debug("Skip CORS processing, response already contains \"Access-Control-Allow-Origin\" header");
            return true;
        }
        final boolean preFlightRequest = CorsUtils.isPreFlightRequest(request);
        if (config == null) {
            if (preFlightRequest) {
                rejectRequest(response);
                return false;
            } else {
                return true;
            }
        }
        return handleInternal(request, response, config, preFlightRequest);
    }

    private boolean responseHasCors(HttpServletResponse response) {
        boolean hasAllowOrigin = false;
        try {
            hasAllowOrigin = (response.getHeader(CorsHttpHeaders.ACCESS_CONTROL_ALLOW_ORIGIN) != null);
        } catch (final NullPointerException npe) {
            // SPR-11919 and https://issues.jboss.org/browse/WFLY-3474
        }
        return hasAllowOrigin;
    }

    /**
     * Invoked when one of the CORS checks failed. The default implementation sets the response status to 403 and writes
     * "Invalid CORS request" to the response.
     */
    protected void rejectRequest(HttpServletResponse response) throws IOException {
        response.setStatus(HttpStatus.FORBIDDEN.value());
        response.getWriter().write("Invalid CORS request");
    }

    /**
     * Handle the given request.
     */
    protected boolean handleInternal(HttpServletRequest request, HttpServletResponse response,
                                     CorsConfiguration config, boolean preFlightRequest) throws IOException {
        final String requestOrigin = request.getHeader(HttpHeaders.ORIGIN);
        final String allowOrigin = checkOrigin(config, requestOrigin);
        final HttpMethod requestMethod = getMethodToUse(request, preFlightRequest);
        final List<HttpMethod> allowMethods = checkMethods(config, requestMethod);
        final List<String> requestHeaders = getHeadersToUse(request, preFlightRequest);
        final List<String> allowHeaders = checkHeaders(config, requestHeaders);
        if (allowOrigin == null || allowMethods == null || (preFlightRequest && allowHeaders == null)) {
            rejectRequest(response);
            return false;
        }
        response.setHeader(CorsHttpHeaders.ACCESS_CONTROL_ALLOW_ORIGIN, allowOrigin);
        response.addHeader(HttpHeaders.VARY, HttpHeaders.ORIGIN);
        if (preFlightRequest) {
            response.setHeader(CorsHttpHeaders.ACCESS_CONTROL_ALLOW_METHODS,
                    StringUtils.collectionToCommaDelimitedString(allowMethods));
        }
        if (preFlightRequest && !allowHeaders.isEmpty()) {
            response.setHeader(CorsHttpHeaders.ACCESS_CONTROL_ALLOW_HEADERS, toCommaDelimitedString(allowHeaders));
        }
        if (!CollectionUtils.isEmpty(config.getExposedHeaders())) {
            response.setHeader(CorsHttpHeaders.ACCESS_CONTROL_EXPOSE_HEADERS,
                    toCommaDelimitedString(config.getExposedHeaders()));
        }
        if (Boolean.TRUE.equals(config.getAllowCredentials())) {
            response.setHeader(CorsHttpHeaders.ACCESS_CONTROL_ALLOW_CREDENTIALS, Boolean.toString(true));
        }
        if (preFlightRequest && config.getMaxAge() != null) {
            response.setHeader(CorsHttpHeaders.ACCESS_CONTROL_MAX_AGE, Long.toString(config.getMaxAge()));
        }

        return true;
    }

    /**
     * Check the origin and determine the origin for the response. The default implementation simply delegates to
     * {@link org.springframework.web.cors.CorsConfiguration#checkOrigin(String)}
     */
    protected String checkOrigin(CorsConfiguration config, String requestOrigin) {
        return config.checkOrigin(requestOrigin);
    }

    /**
     * Check the HTTP method and determine the methods for the response of a pre-flight request. The default
     * implementation simply delegates to {@link org.springframework.web.cors.CorsConfiguration#checkOrigin(String)}
     */
    protected List<HttpMethod> checkMethods(CorsConfiguration config, HttpMethod requestMethod) {
        return config.checkHttpMethod(requestMethod);
    }

    private HttpMethod getMethodToUse(HttpServletRequest request, boolean isPreFlight) {
        final String value = request.getHeader(CorsHttpHeaders.ACCESS_CONTROL_REQUEST_METHOD);
        return (isPreFlight ? (value != null ? HttpMethod.valueOf(value) : null) : HttpMethod.valueOf(request
                .getMethod()));
    }

    /**
     * Check the headers and determine the headers for the response of a pre-flight request. The default implementation
     * simply delegates to {@link org.springframework.web.cors.CorsConfiguration#checkOrigin(String)}
     */
    protected List<String> checkHeaders(CorsConfiguration config, List<String> requestHeaders) {
        return config.checkHeaders(requestHeaders);
    }

    private List<String> getHeadersToUse(HttpServletRequest request, boolean isPreFlight) {
        return (isPreFlight ? getFirstValueAsList(request, CorsHttpHeaders.ACCESS_CONTROL_REQUEST_HEADERS)
                : new ArrayList<String>(Collections.list(request.getHeaderNames())));
    }

    /**
     * Check if the request is a same-origin one, based on {@code Origin}, {@code Host}, {@code Forwarded} and
     * {@code X-Forwarded-Host} headers.
     * 
     * @return {@code true} if the request is a same-origin one, {@code false} in case of cross-origin request.
     * @throws URISyntaxException
     * @since 4.2
     */
    private static boolean isSameOrigin(HttpServletRequest request) {
        final String origin = request.getHeader(HttpHeaders.ORIGIN);
        if (origin == null) {
            return true;
        }
        UriComponents actualUrl;
        try {
            actualUrl = fromHttpRequest(request).build();
        } catch (final URISyntaxException e) {
            LOGGER.error("URISyntaxException", e);
            return false;
        }
        final UriComponents originUrl = UriComponentsBuilder.fromOriginHeader(origin).build();
        return (actualUrl.getHost().equals(originUrl.getHost()) && getPort(actualUrl) == getPort(originUrl));
    }

    private static int getPort(UriComponents component) {
        int port = component.getPort();
        if (port == -1) {
            if ("http".equals(component.getScheme()) || "ws".equals(component.getScheme())) {
                port = 80;
            } else if ("https".equals(component.getScheme()) || "wss".equals(component.getScheme())) {
                port = 443;
            }
        }
        return port;
    }

    protected static List<String> getFirstValueAsList(HttpServletRequest request, String header) {
        final List<String> result = new ArrayList<String>();
        final String value = request.getHeader(header);
        if (value != null) {
            final String[] tokens = value.split(",\\s*");
            for (final String token : tokens) {
                result.add(token);
            }
        }
        return result;
    }

    protected static String toCommaDelimitedString(List<String> list) {
        final StringBuilder builder = new StringBuilder();
        for (final Iterator<String> iterator = list.iterator(); iterator.hasNext();) {
            final String ifNoneMatch = iterator.next();
            builder.append(ifNoneMatch);
            if (iterator.hasNext()) {
                builder.append(", ");
            }
        }
        return builder.toString();
    }

    public static UriComponentsBuilder fromHttpRequest(HttpServletRequest request) throws URISyntaxException {
        final URI uri = new URI(request.getRequestURL().toString());
        final UriComponentsBuilder builder = UriComponentsBuilder.fromUri(uri);

        String scheme = uri.getScheme();
        String host = uri.getHost();
        int port = uri.getPort();

        final String hostHeader = request.getHeader("X-Forwarded-Host");
        if (StringUtils.hasText(hostHeader)) {
            final String[] hosts = StringUtils.commaDelimitedListToStringArray(hostHeader);
            final String hostToUse = hosts[0];
            if (hostToUse.contains(":")) {
                final String[] hostAndPort = StringUtils.split(hostToUse, ":");
                host = hostAndPort[0];
                port = Integer.parseInt(hostAndPort[1]);
            } else {
                host = hostToUse;
                port = -1;
            }
        }

        final String portHeader = request.getHeader("X-Forwarded-Port");
        if (StringUtils.hasText(portHeader)) {
            final String[] ports = StringUtils.commaDelimitedListToStringArray(portHeader);
            port = Integer.parseInt(ports[0]);
        }

        final String protocolHeader = request.getHeader("X-Forwarded-Proto");
        if (StringUtils.hasText(protocolHeader)) {
            final String[] protocols = StringUtils.commaDelimitedListToStringArray(protocolHeader);
            scheme = protocols[0];
        }

        builder.scheme(scheme);
        builder.host(host);
        builder.port(null);
        if ("http".equals(scheme) && port != 80 || "https".equals(scheme) && port != 443) {
            builder.port(port);
        }
        return builder;
    }

}
