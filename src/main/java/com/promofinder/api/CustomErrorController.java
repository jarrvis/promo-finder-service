package com.promofinder.api;


import java.util.Map;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import com.promofinder.ApiConfiguration;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.autoconfigure.web.ErrorAttributes;
import org.springframework.boot.autoconfigure.web.ErrorController;
import org.springframework.http.MediaType;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.context.request.RequestAttributes;
import org.springframework.web.context.request.ServletRequestAttributes;

/**
 * Based on the helpful answer at http://stackoverflow.com/q/25356781/56285, with error details in response body added.
 *
 * https://gist.github.com/jonikarppinen/662c38fb57a23de61c8b
 *
 * @author Joni Karppinen
 * @since 20.2.2015
 */
@RestController
public class CustomErrorController implements ErrorController {

    private static final String PATH = "/error";

    @Autowired
    ApiConfiguration apiConfiguration;

    @Autowired
    ErrorAttributes errorAttributes;

    /**
     * Request handler for the /error endpoint.
     *
     * @param request
     *            HttpServletRequest
     * @param response
     *            HttpServletResponse
     * @return ErrorJson object
     */
    @RequestMapping(value = PATH, produces = MediaType.APPLICATION_JSON_VALUE)
    ErrorJson error(HttpServletRequest request, HttpServletResponse response) {
        // Appropriate HTTP response code (e.g. 404 or 500) is automatically set by Spring.
        // Here we just define response body.
        return new ErrorJson(response.getStatus(), getErrorAttributes(request,
                apiConfiguration.getIncludeStackTraceOnErrorPage()));
    }

    @Override
    public String getErrorPath() {
        return PATH;
    }

    private Map<String, Object> getErrorAttributes(HttpServletRequest request, boolean includeStackTrace) {
        final RequestAttributes requestAttributes = new ServletRequestAttributes(request);
        return errorAttributes.getErrorAttributes(requestAttributes, includeStackTrace);
    }

    /**
     * Wrapper class for JSON output of errors.
     *
     */
    public class ErrorJson {

        private final Integer status;
        private final String error;
        private final String message;
        private final String timeStamp;
        private final String trace;

        public ErrorJson(int status, Map<String, Object> errorAttributes) {
            this.status = status;
            this.error = (String) errorAttributes.get("error");
            this.message = (String) errorAttributes.get("message");
            this.timeStamp = errorAttributes.get("timestamp").toString();
            this.trace = (String) errorAttributes.get("trace");
        }

        /**
         * Return error status.
         *
         * @return error status
         */
        public Integer getStatus() {
            return status;
        }

        /**
         * Return the error itself.
         *
         * @return error string
         */
        public String getError() {
            return error;
        }

        /**
         * Return error message.
         *
         * @return error message string.
         */
        public String getMessage() {
            return message;
        }

        /**
         * Return error timestamp.
         *
         * @return error timestamp string
         */
        public String getTimeStamp() {
            return timeStamp;
        }

        /**
         * Return error stacktrace.
         *
         * @return error stacktrace string.
         */
        public String getTrace() {
            return trace;
        }
    }

}
