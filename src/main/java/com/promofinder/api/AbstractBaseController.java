package com.promofinder.api;

import com.google.common.primitives.Ints;
import com.promofinder.api.exception.InvalidRequestException;
import com.promofinder.api.exception.NotAcceptableException;
import com.promofinder.model.ValidParam;
import com.promofinder.model.ValidationError;
import org.apache.commons.lang3.StringUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.MessageSource;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.domain.Sort;
import org.springframework.http.HttpStatus;
import org.springframework.validation.FieldError;
import org.springframework.web.bind.annotation.ExceptionHandler;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.bind.annotation.ResponseStatus;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;
import java.util.Optional;


/**
 * Created by KAI8WZ on 25.09.2017.
 */
public class AbstractBaseController {

    private static final Logger LOG = LoggerFactory.getLogger(AbstractBaseController.class);

    @Autowired
    MessageSource messageSource;

    Optional<PageRequest> getPageRequestOptional(final String limit, final String sortBy, final Class clazz) {

        Optional<Sort> sort = Optional.empty();
        Optional<Integer> resultsLimit = Optional.empty();

        if (clazz != null && StringUtils.isNotEmpty(sortBy)) {
            final boolean reverseSort = sortBy.startsWith("-");
            final String sortByField = StringUtils.removeStart(sortBy,"-");
            sort = Optional.ofNullable(Arrays.stream(clazz.getDeclaredFields()).anyMatch(field -> field.getName().equals(sortByField)) ?
                    new Sort(reverseSort ? Sort.Direction.ASC : Sort.Direction.DESC, sortByField) : null);

            if (!sort.isPresent())
                throw new NotAcceptableException("Invalid sortBy parameter.");
        }

        if (StringUtils.isNotEmpty(limit)) {
            resultsLimit = Optional.ofNullable(Ints.tryParse(limit));
            if (!resultsLimit.isPresent())
                throw new NotAcceptableException("Invalid limit parameter.");
        }

        return Optional.ofNullable(sort.isPresent() && resultsLimit.isPresent() ?
                new PageRequest(0, resultsLimit.get(), sort.get()) :
                (sort.isPresent() || resultsLimit.isPresent() ?
                        (sort.isPresent() ?
                                new PageRequest(0, Integer.MAX_VALUE, sort.get()) :
                                new PageRequest(0, resultsLimit.get())
                        ) : null));
    }

    /**
     * Exception handler for <i>InvalidRequestException</i>, translating error into HTTP status code 422 and returning
     * validation errors in the payload of the response.
     *
     * @param ex
     *            InvalidRequestException
     */
    @ResponseStatus(value = HttpStatus.UNPROCESSABLE_ENTITY)
    @ExceptionHandler(InvalidRequestException.class)
    @ResponseBody
    public ValidParam handleInvalidRequestException(InvalidRequestException ex) {
        LOG.warn(ex.getMessage());

        final List<ValidationError> validationErrors = new ArrayList<>();
        ex.getErrors().getFieldErrors().forEach(fieldError -> {
            LOG.debug("Error in field {}: {}", fieldError.getField(),
                    messageSource.getMessage(fieldError, ex.getLocale()));

            validationErrors.add(new ValidationError(fieldError.getField(), fieldError.getCode(),
                    messageSource.getMessage(fieldError, ex.getLocale())));
        });

        final ValidParam response = new ValidParam();
        response.setValidationErrors(validationErrors);

        return response;
    }
}
