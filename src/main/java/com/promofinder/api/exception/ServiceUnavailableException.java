package com.promofinder.api.exception;

/**
 * Created by KAI8WZ on 04.10.2017.
 */
public class ServiceUnavailableException extends RuntimeException {

    private static final long serialVersionUID = 6872580404755827065L;

    /**
     * Constructor accepting an error message.
     *
     * @param message message
     */
    public ServiceUnavailableException(String message) {
        super(message);
    }
}
