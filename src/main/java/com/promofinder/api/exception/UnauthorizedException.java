package com.promofinder.api.exception;

import org.springframework.http.HttpStatus;

/**
 * 
 * Exception that is thrown if the user is unauthorized. Currently this
 * exception is not handled as the other exceptions. Because the @ResponseEntityExceptionHandler
 * cannot handle exceptions that are thrown in a filter this exception is
 * currently only misused to hold the static data
 * 
 * @author FBS8FE
 *
 */
public class UnauthorizedException extends RestAPIException {

    /**
     * Constructor to create a new Unauthorized Exception.
     * 
     * @param detailedExceptionMessage
     */
    public UnauthorizedException(String detailedExceptionMessage) {

        super(CODE, MESSAGE_ENGLISH, MESSAGE_GERMAN, detailedExceptionMessage);
    }

    private static final long serialVersionUID = -2280214438467968290L;
    static final HttpStatus CODE = HttpStatus.UNAUTHORIZED;
    static final String MESSAGE_ENGLISH =
            "Not authorized. The used api key is either not valid, does not exists or is missing.";
    static final String MESSAGE_GERMAN =
            "Nicht authentifiziert. Der verwendete API-Key ist ungültig, fehlt oder existiert nicht.";

    // public static HttpStatus getCode() {
    // return CODE;
    // }

    // public static String getMessageEnglish() {
    // return MESSAGE_ENGLISH;
    // }
    //
    // public static String getMessageGerman() {
    // return MESSAGE_GERMAN;
    // }

}
