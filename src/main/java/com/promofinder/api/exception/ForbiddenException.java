package com.promofinder.api.exception;

/**
 * eShop API exception, thrown when eg. an authentication attempt is not allowed with the given credentials (not details
 * returned).
 * 
 * @author rot8le
 *
 */
public class ForbiddenException extends RuntimeException {

    private static final long serialVersionUID = 2365475115651958346L;

    /**
     * Constructor accepting an error message.
     * 
     * @param message
     *            message
     */
    public ForbiddenException(String message) {
        super(message);
    }

}
