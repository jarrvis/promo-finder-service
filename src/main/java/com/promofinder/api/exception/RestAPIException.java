package com.promofinder.api.exception;

import io.swagger.annotations.ApiModelProperty;
import org.springframework.http.HttpStatus;

import java.util.Date;

/**
 * The super type for exceptions that can occur during the communication with
 * this service.
 * 
 * @author Benjamin Fischer (Benjamin.Fischer@de.bosch.com)
 *
 */
public class RestAPIException extends RuntimeException {

    private static final long serialVersionUID = 5350475397967815294L;

    private final HttpStatus code;
    private final String messageGerman;
    private final String messageEnglish;
    private final String detailedMessage;
    private final long timestamp;

    // This is necessary to hide the information on the Swagger UI
    @ApiModelProperty(
            hidden = true)
    static final Throwable CAUSE = null;
    @ApiModelProperty(
            hidden = true)
    static final Throwable STACKTRACE = null;
    @ApiModelProperty(
            hidden = true)
    static final Throwable SUPPRESSED = null;
    @ApiModelProperty(
            hidden = true)
    static final String LOCALIZEDMESSAGE = null;
    @ApiModelProperty(
            hidden = true)
    static final String MESSAGE = null;

    /**
     * Construct a new exception of this type.
     * 
     * @param code2
     * @param messageEnglisch2
     * @param messageGerman2
     * @param dm
     */
    public RestAPIException(HttpStatus code2, String messageEnglisch2,
                            String messageGerman2, String dm) {

        code = code2;
        messageEnglish = messageEnglisch2;
        messageGerman = messageGerman2;
        timestamp = new Date().getTime();
        detailedMessage = dm;

    }

    public HttpStatus getCode() {
        return code;
    }

    public String getMessageGerman() {
        return messageGerman;
    }

    public String getMessageEnglish() {
        return messageEnglish;
    }

    public long getTimestamp() {
        return timestamp;
    }

    public String getDetailedMessage() {
        return detailedMessage;
    }

}
