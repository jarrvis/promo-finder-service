package com.promofinder.model;

import com.fasterxml.jackson.annotation.JsonIgnore;
import com.fasterxml.jackson.annotation.JsonProperty;

import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;

import java.io.Serializable;
import java.math.BigDecimal;
import java.util.List;

import org.apache.commons.lang3.builder.ToStringBuilder;
import org.dom4j.tree.AbstractEntity;
import org.hibernate.annotations.GenericGenerator;

import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;

/**
 * Model representation of a product sold via the IoT eShop service.
 *
 * @author rot8le
 *
 */
@ApiModel(description = "Model representation of a product sold via the IoT eShop service")
@Entity
public class Product implements Serializable {

    private static final long serialVersionUID = 8269473897901384963L;

    @Id
    @GeneratedValue(generator = "uuid")
    @GenericGenerator(name = "uuid", strategy = "uuid2")
    @ApiModelProperty(value = "The unqiue id of the product", required = true)
    @JsonProperty("id")
    private String id;

    @ApiModelProperty(value = "The name of the product. E.g \"TrackMyTools Basic month\"", required = true)
    @JsonProperty("name")
    private String name;

    @ApiModelProperty(value = "The long description of the product", required = true)
    @JsonProperty("description")
    private String description;

    @ApiModelProperty(value = "The net price per month.", required = true)
    @JsonProperty("netPriceMonth")
    private BigDecimal netPriceMonth;

    @ApiModelProperty(value = "The gross price per month.", required = true)
    @JsonProperty("grossPriceMonth")
    private BigDecimal grossPriceMonth;

    @ApiModelProperty(value = "The ISO 4217 currency code for the net price", required = true)
    @JsonProperty("currencyCode")
    private String currencyCode;

    @ApiModelProperty(value = "VAT rate applicable for the product", required = true)
    @JsonProperty("vatRate")
    private Integer vatPercentage;

    @ApiModelProperty(value = "The duration of a subscription contract in months (eg. 12).", required = true)
    @JsonProperty("contractDuration")
    private Integer contractDuration;

    @ApiModelProperty(value = "The duration of an (automatic) follow-up contract.", required = true)
    @JsonProperty("followUpContractDuration")
    private Integer followUpContractDuration;

    @ApiModelProperty(value = "The cancellation period in days to end the contract.", required = true)
    @JsonProperty("cancellationPeriod")
    private Integer cancellationPeriod;

    @ApiModelProperty(value = "The maximum amount of Track-Tags that can be managed with this product type.", required = true)
    @JsonProperty("maxConnectedDevices")
    private Long maxConnectedDevices;

    @ApiModelProperty(value = "The unique id of the according Start-Kit Product, if applicable. Used to create the free trial period.")
    @JsonProperty("linkedStarterKitProductId")
    private String linkedStarterKitProductId;

    @ApiModelProperty(value = "The unique id of the according Test-Box Product, if applicable. Used to create the free trial period.")
    @JsonProperty("linkedTestBoxProductId")
    private String linkedTestBoxProductId;

    @ApiModelProperty(value = "The unique id of the according add-on product for a Test-Box Product, if applicable.")
    @JsonProperty("linkedAddonProductId")
    private String linkedAddonProductId;

    @ApiModelProperty(value = "The flag whether the product shall be buyable in the shop or not.", required = true)
    @JsonProperty("purchasable")
    private boolean shopPurchasable;

    @ApiModelProperty(value = "The flag whether the product shall be listed in the eShop interface or not.", required = true)
    @JsonIgnore
    private boolean shopVisible;

    @ApiModelProperty(value = "The flag whether the product is a freemium product or not.", required = true)
    @JsonProperty("freemium")
    private boolean freemium;

    @ApiModelProperty(value = "The flag whether the product is a TestBox product or not.", required = true)
    @JsonIgnore
    private boolean testBox;

    @ApiModelProperty(value = "The flag whether the product is a StarterKit product or not.", required = true)
    @JsonIgnore
    private boolean starterKit;

    @ApiModelProperty(value = "The maximum amount of megabytes which user can upload.", required = true)
    @JsonProperty("dataUploadLimit")
    private Long dataUploadLimit;

    /**
     * Get the id of the entity.
     *
     * @return entity id
     */
    public String getId() {
        return id;
    }

    /**
     * Set the id of the entity.
     *
     * @param id
     *            entity id
     */
    public void setId(String id) {
        this.id = id;
    }

    /**
     * Return the product name.
     *
     * @return The product name
     */
    public String getName() {
        return name;
    }

    /**
     * Set the product name.
     *
     * @param name
     *            The product name
     */
    public void setName(String name) {
        this.name = name;
    }

    /**
     * Return the product description.
     *
     * @return The product description
     */
    public String getDescription() {
        return description;
    }

    /**
     * Set the product description.
     *
     * @param description
     *            The product description
     */
    public void setDescription(String description) {
        this.description = description;
    }

    /**
     * Return the net price per month.
     *
     * @return The netPriceMonth
     */

    public BigDecimal getNetPriceMonth() {
        return netPriceMonth;
    }

    /**
     * Set the net price per month.
     *
     * @param netPriceMonth
     *            The netPriceMonth
     */
    public void setNetPriceMonth(BigDecimal netPriceMonth) {
        this.netPriceMonth = netPriceMonth;
    }

    /**
     * Return the gross price per month.
     *
     * @return The grossPriceMonth
     */
    public BigDecimal getGrossPriceMonth() {
        return grossPriceMonth;
    }

    /**
     * Set the gross price per month.
     *
     * @param grossPriceMonth
     *            The grossPriceMonth
     */
    public void setGrossPriceMonth(BigDecimal grossPriceMonth) {
        this.grossPriceMonth = grossPriceMonth;
    }

    /**
     * Return the currency code.
     *
     * @return The currencyCode
     */
    public String getCurrencyCode() {
        return currencyCode;
    }

    /**
     * Set the currency code.
     *
     * @param currencyCode
     *            The currencyCode
     */
    public void setCurrencyCode(String currencyCode) {
        this.currencyCode = currencyCode;
    }

    /**
     * Return the VAT rate.
     *
     * @return The VAT rate
     */
    public Integer getVatPercentage() {
        return vatPercentage;
    }

    /**
     * Set the VAT rate.
     *
     * @param vatPercentage
     *            The VAT rate
     */
    public void setVatPercentage(Integer vatPercentage) {
        this.vatPercentage = vatPercentage;
    }

    /**
     * Return the contract duration.
     *
     * @return The contractDuration
     */
    public Integer getContractDuration() {
        return contractDuration;
    }

    /**
     * Set the contract duration.
     *
     * @param contractDuration
     *            The contractDuration
     */
    public void setContractDuration(Integer contractDuration) {
        this.contractDuration = contractDuration;
    }

    /**
     * Return the followup contract duration.
     *
     * @return The followUpContractDuration
     */
    public Integer getFollowUpContractDuration() {
        return followUpContractDuration;
    }

    /**
     * Set the followup contract duration.
     *
     * @param followUpContractDuration
     *            The followUpContractDuration
     */
    public void setFollowUpContractDuration(Integer followUpContractDuration) {
        this.followUpContractDuration = followUpContractDuration;
    }

    /**
     * Return the cancellation period.
     *
     * @return The cancellationPeriod
     */
    public Integer getCancellationPeriod() {
        return cancellationPeriod;
    }

    /**
     * Set the cancellation period.
     *
     * @param cancellationPeriod
     *            The cancellationPeriod
     */
    public void setCancellationPeriod(Integer cancellationPeriod) {
        this.cancellationPeriod = cancellationPeriod;
    }

    /**
     * Return the max number of connected devices.
     *
     * @return The maxConnectedDevices
     */
    public Long getMaxConnectedDevices() {
        return maxConnectedDevices;
    }

    /**
     * Set the max number of connected devices.
     *
     * @param maxConnectedDevices
     *            The maxConnectedDevices
     */
    public void setMaxConnectedDevices(Long maxConnectedDevices) {
        this.maxConnectedDevices = maxConnectedDevices;
    }

    /**
     * Return the linked StartKit product id.
     *
     * @return The linkedStarterKitProductId
     */
    public String getLinkedStarterKitProductId() {
        return linkedStarterKitProductId;
    }

    /**
     * Set the linked StarterKit product id.
     *
     * @param linkedStarterKitProductId
     *            The linkedStarterKitProductId
     */
    public void setLinkedStarterKitProductId(String linkedStarterKitProductId) {
        this.linkedStarterKitProductId = linkedStarterKitProductId;
    }

    /**
     * Return the linked TestBox product id.
     *
     * @return The linkedTestBoxProductId
     */
    public String getLinkedTestBoxProductId() {
        return linkedTestBoxProductId;
    }

    /**
     * Set the linked TestBox product id.
     *
     * @param linkedTestBoxProductId
     *            The linkedTestBoxProductId
     */
    public void setLinkedTestBoxProductId(String linkedTestBoxProductId) {
        this.linkedTestBoxProductId = linkedTestBoxProductId;
    }

    /**
     * Return the linked add-on product id.
     *
     * @return The linkedAddonProductId
     */
    public String getLinkedAddonProductId() {
        return linkedAddonProductId;
    }

    /**
     * Set the linked add-on product id.
     *
     * @param linkedAddonProductId
     *            The linkedAddonProductId
     */
    public void setLinkedAddonProductId(String linkedAddonProductId) {
        this.linkedAddonProductId = linkedAddonProductId;
    }

    /**
     * Return the flag if this product is purchasable.
     *
     * @return The shopPurchasable
     */
    public boolean getShopPurchasable() {
        return shopPurchasable;
    }

    /**
     * Java-style version of the getShopPurchasable method.
     *
     * @return The shopPurchasable flag
     */
    @JsonIgnore
    public boolean isPurchasable() {
        return getShopPurchasable();
    }

    /**
     * Set the flag if this product is purchasable.
     *
     * @param shopPurchasable
     *            The shopPurchasable
     */
    public void setShopPurchasable(boolean shopPurchasable) {
        this.shopPurchasable = shopPurchasable;
    }

    /**
     * Return the flag if this product is visible in the shop.
     *
     * @return The shopVisible flag
     */
    public boolean getShopVisible() {
        return shopVisible;
    }

    /**
     * Java-style version of the getShopPurchasable method.
     *
     * @return The shopVisible flag
     */
    @JsonIgnore
    public boolean isVisible() {
        return getShopVisible();
    }

    /**
     * Set the flag if this product is visible in the shop.
     *
     * @param shopVisible
     *            The shopVisible
     */
    public void setShopVisible(boolean shopVisible) {
        this.shopVisible = shopVisible;
    }

    /**
     * Return the flag if this product is a freemium product.
     *
     * @return The freemium flag
     */
    public Boolean getFreemium() {
        return freemium;
    }

    /**
     * Set the flag if this product is a freemium product.
     *
     * @param freemium
     *            The freemium flag
     */
    public void setFreemium(Boolean freemium) {
        this.freemium = freemium;
    }

    /**
     * Java-style version of the getFreemium method.
     *
     * @return freemium product flag
     */
    @JsonIgnore
    public boolean isFreemium() {
        return getFreemium();
    }

    /**
     * Return the flag if this product is a TestBox product.
     *
     * @return The testBox flag
     */
    public boolean getTestBox() {
        return testBox;
    }

    /**
     * Set the flag if this product is a TestBox product.
     *
     * @param testBox
     *            The testBox flag
     */
    public void setTestBox(boolean testBox) {
        this.testBox = testBox;
    }

    /**
     * Java-style version of the getTestBox method.
     *
     * @return testBox flag
     */
    @JsonIgnore
    public boolean isTestBox() {
        return this.getTestBox();
    }

    /**
     * Return the flag if this product is a StarterKit product.
     *
     * @return The starterKit flag
     */
    public boolean getStarterKit() {
        return starterKit;
    }

    /**
     * Set the flag if this product is a StarterKit product.
     *
     * @param starterKit
     *            The starterKit flag
     */
    public void setStarterKit(boolean starterKit) {
        this.starterKit = starterKit;
    }

    /**
     * Java-style version of the getStarterKit method.
     *
     * @return starterKit flag
     */
    @JsonIgnore
    public boolean isStarterKit() {
        return this.getStarterKit();
    }

    /**
     * Return uploadable data limit.
     * Maximum amount of megabytes of data which user can upload.
     *
     * @return data upload limit in megabytes
     */
    public Long getDataUploadLimit() {
        return this.dataUploadLimit;
    }

    /**
     * Set upload data limit.
     * Maximum amount of megabytes of data which user can upload.
     *
     * @param dataUploadLimit data upload limit.
     */
    public void setDataUploadLimit(final Long dataUploadLimit) {
        this.dataUploadLimit = dataUploadLimit;
    }

}
