package com.promofinder.model;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import org.springframework.http.HttpStatus;

/**
 * The resource that represents the envelope of an error.
 * 
 * @author FBS8FE
 *
 */
@JsonIgnoreProperties(ignoreUnknown = true)
public class ErrorResource {
    private HttpStatus httpStatusMessage;
    private int httpStatusCode;
    private long timestamp;
    private String messageGerman;
    private String messageEnglish;
    private String detailedMessage;

    public ErrorResource() {
    }

    /**
     * Construct a new response with a detailed error message.
     * 
     * @param httpStatusMessage
     *            : The message of the HTTP status code
     * @param httpStatusCode
     *            : The HTTP status code
     * @param timestamp
     *            : The timestamp of the underlying exception
     * @param messageGerman
     *            : The German message that explains the error to the client
     * @param messageEnglish
     *            : The English message that explains the error the the client
     * @param detailedMessage
     *            : The detailed message from the server
     */
    public ErrorResource(HttpStatus httpStatusMessage, int httpStatusCode,
                         long timestamp, String messageGerman, String messageEnglish,
                         String detailedMessage) {
        super();
        this.httpStatusMessage = httpStatusMessage;
        this.httpStatusCode = httpStatusCode;
        this.timestamp = timestamp;
        this.messageGerman = messageGerman;
        this.messageEnglish = messageEnglish;
        this.setDetailedMessage(detailedMessage);
    }

    /**
     * Construct a new response wihtout a detailed error message.
     * 
     * @param httpStatusMessage
     *            : The message of the HTTP status code
     * @param httpStatusCode
     *            : The HTTP status code
     * @param timestamp
     *            : The timestamp of the underlying exception
     * @param messageGerman
     *            : The German message that explains the error to the client
     * @param messageEnglish
     *            : The English message that explains the error the the client
     */
    public ErrorResource(HttpStatus httpStatusMessage, int httpStatusCode,
                         long timestamp, String messageGerman, String messageEnglish) {
        super();
        this.httpStatusMessage = httpStatusMessage;
        this.httpStatusCode = httpStatusCode;
        this.timestamp = timestamp;
        this.messageGerman = messageGerman;
        this.messageEnglish = messageEnglish;

    }

    public HttpStatus getHttpStatusMessage() {
        return httpStatusMessage;
    }

    public void setHttpStatusMessage(HttpStatus httpStatusMessage) {
        this.httpStatusMessage = httpStatusMessage;
    }

    public int getHttpStatusCode() {
        return httpStatusCode;
    }

    public void setHttpStatusCode(int httpStatusCode) {
        this.httpStatusCode = httpStatusCode;
    }

    public long getTimestamp() {
        return timestamp;
    }

    public void setTimestamp(long timestamp) {
        this.timestamp = timestamp;
    }

    public String getMessageGerman() {
        return messageGerman;
    }

    public void setMessageGerman(String messageGerman) {
        this.messageGerman = messageGerman;
    }

    public String getMessageEnglish() {
        return messageEnglish;
    }

    public void setMessageEnglish(String messageEnglish) {
        this.messageEnglish = messageEnglish;
    }

    public String getDetailedMessage() {
        return detailedMessage;
    }

    public void setDetailedMessage(String detailedMessage) {
        this.detailedMessage = detailedMessage;
    }

}
