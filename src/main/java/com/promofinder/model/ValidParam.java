package com.promofinder.model;

import com.fasterxml.jackson.annotation.JsonProperty;
import io.swagger.annotations.ApiModelProperty;
import org.apache.commons.lang3.builder.ToStringBuilder;

import java.io.Serializable;
import java.util.List;

/**
 * Created by KAI8WZ on 27.09.2017.
 */
public class ValidParam implements Serializable {

    @ApiModelProperty(value = "A list of form attributes that failed to validate.")
    @JsonProperty(value = "validationErrors")
    private  List<ValidationError> validationErrors;

    public List<ValidationError> getValidationErrors() {
        return this.validationErrors;
    }

    public void setValidationErrors(List<ValidationError> validationErrors) {
        this.validationErrors = validationErrors;
    }

    @Override
    public String toString() {
        return ToStringBuilder.reflectionToString(this);
    }

}
