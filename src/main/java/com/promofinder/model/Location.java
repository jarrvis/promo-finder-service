package com.promofinder.model;

import com.fasterxml.jackson.annotation.JsonIgnore;
import io.swagger.annotations.ApiModelProperty;
import org.apache.commons.lang3.builder.ToStringBuilder;
import org.dom4j.tree.AbstractEntity;
import org.hibernate.annotations.GenericGenerator;
import org.hibernate.validator.constraints.NotEmpty;

import javax.persistence.*;
import javax.validation.constraints.NotNull;
import javax.validation.constraints.Size;
import java.io.Serializable;
import java.util.HashMap;

/**
 * Created by KAI8WZ on 11.06.2017.
 */


@Entity
public class Location implements Serializable {

    private static final long serialVersionUID = 8269473897901856963L;

    public static final int MAX_LENGTH_NAME = 50;
    public static final int MAX_LENGTH_CITY = 40;

    @Id
    @GeneratedValue(generator = "uuid")
    @GenericGenerator(name = "uuid", strategy = "uuid2")
    private String id;

    @ApiModelProperty(value = "Name of the location.", required = true)
    @NotEmpty
    @Size(max = MAX_LENGTH_NAME)
    private String name;

    @JsonIgnore
    @ManyToOne
    @JoinColumn(name = "location_data_id", referencedColumnName="ID")
    private LocationData locationData;

    @ApiModelProperty(value = "The longitude of the location.", required = true)
    @NotNull
    private Double longitude;

    @ApiModelProperty(value = "The latitude of the location.", required = true)
    @NotNull
    private Double latitude;

    @ApiModelProperty(value = "First line of address.", required = true)
    @NotEmpty
    private String address1;

    @ApiModelProperty(value = "Second line of address.")
    private String address2;

    @ApiModelProperty(value = "Third line of address.")
    private String address3;

    @ApiModelProperty(value = "Distance from the shop.")
    private double distance;

    protected Location() {}     //for JPA

    public Location(String name, double longitude, double latitude) {
        this.name = name;
        this.longitude = longitude;
        this.latitude = latitude;
    }
    public Location(String name, double longitude, double latitude, String address1, String address2, String address3) {
        this.name = name;
        this.longitude = longitude;
        this.latitude = latitude;
        this.address1 = address1;
        this.address2 = address2;
        this.address3 = address3;
    }
    public Location(HashMap<String,String> map) {

        this.name = map.get("name");
        this.longitude = Double.parseDouble(map.get("longitude"));
        this.latitude = Double.parseDouble(map.get("latitude"));
        this.address1 = map.get("address1");
        this.address2 = map.get("address2");
        this.address3 = map.get("address3");

    }

    public Location(String id) {
        this.id = id;
    }

    public String getId() {
        return id;
    }

    public String getName() {
        return name;
    }

    public double getLongitude() {
        return longitude;
    }

    public double getLatitude() {
        return latitude;
    }

    public String getAddress1() {
        return address1;
    }

    public String getAddress2() {
        return address2;
    }

    public String getAddress3() {
        return address3;
    }

    public double getDistance() {
        return distance;
    }

    public void setName(String name) {
        this.name = name;
    }

    public LocationData getLocationData() {
        return locationData;
    }

    public void setLocationData(LocationData locationData) {
        this.locationData = locationData;
    }

    @Override
    public String toString() {
        return ToStringBuilder.reflectionToString(this);
    }

}
