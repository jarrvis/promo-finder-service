package com.promofinder.model;

import com.fasterxml.jackson.annotation.JsonIgnore;
import io.swagger.annotations.ApiModelProperty;
import org.hibernate.annotations.GenericGenerator;
import org.hibernate.validator.constraints.NotEmpty;
import org.hibernate.validator.constraints.URL;

import javax.persistence.*;
import javax.validation.constraints.NotNull;
import javax.validation.constraints.Size;
import java.io.Serializable;
import java.sql.Timestamp;
import java.time.LocalDate;
import java.util.List;

/**
 * Created by KAI8WZ on 01.10.2017.
 */
@Entity
@Table(name="location_data")
public class LocationData implements Serializable{

    private static final int MAX_LENGTH_NAME = 35;

    @Id
    @GeneratedValue(generator = "uuid")
    @GenericGenerator(name = "uuid", strategy = "uuid2")
    @JsonIgnore
    private String id;

    @OneToMany(mappedBy = "locationData", fetch = FetchType.LAZY, cascade = CascadeType.ALL)
    private List<Location> locations;

    @ApiModelProperty(value = "Total count of locations.", required = true)
    @NotNull
    private Integer totalCount;

    @ApiModelProperty(value = "Name of the location.", required = true)
    @NotEmpty
    @Size(max = MAX_LENGTH_NAME)
    private String name;

    @ApiModelProperty(value = "Location image url.")
    private String imageUrl;

    @NotNull
    @Column(nullable = false, name = "created_at", updatable = false)
    private Timestamp createdAt;

    public LocationData(){
    }

    public LocationData(List<Location> locations, String name){
        this.name = name;
        this.locations = locations;
        this.totalCount = locations.size();
        locations.forEach(location -> location.setLocationData(this));
    }
    /**
     * Automatically called by Spring to set a created_at timestamp. This is only performed if no previous value is set.
     */
    @PrePersist
    public void updateTimeStamps() {
        if (createdAt == null) {
            LocalDate localDate = LocalDate.now();
            createdAt = Timestamp.valueOf(localDate.atStartOfDay());
        }
    }

    /**
     * Return the id of this location data.
     *
     * @return id
     */
    public String getId() {
        return id;
    }

    /**
     * Set the id of this location data.
     *
     * @param id
     *            id string
     */
    public void setId(String id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public List<Location> getLocations() {
        return locations;
    }

    public void setLocations(List<Location> locations) {
        this.locations = locations;
    }

    public Integer getTotalCount() {
        return totalCount;
    }

    public void setTotalCount(Integer totalCount) {
        this.totalCount = totalCount;
    }

    public String getImageUrl() {
        return imageUrl;
    }

    public void setImageUrl(String imageUrl) {
        this.imageUrl = imageUrl;
    }
}
