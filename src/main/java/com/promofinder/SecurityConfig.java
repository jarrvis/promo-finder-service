package com.promofinder;

import com.promofinder.api.ApiController;
import com.promofinder.api.filter.AuthenticationFilter;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.core.annotation.Order;
import org.springframework.http.HttpMethod;
import org.springframework.security.config.annotation.authentication.builders.AuthenticationManagerBuilder;
import org.springframework.security.config.annotation.method.configuration.EnableGlobalMethodSecurity;
import org.springframework.security.config.annotation.web.builders.HttpSecurity;
import org.springframework.security.config.annotation.web.configuration.EnableWebSecurity;
import org.springframework.security.config.annotation.web.configuration.WebSecurityConfigurerAdapter;
import org.springframework.security.config.http.SessionCreationPolicy;
import org.springframework.security.core.AuthenticationException;
import org.springframework.security.web.AuthenticationEntryPoint;
import org.springframework.security.web.authentication.www.BasicAuthenticationEntryPoint;
import org.springframework.security.web.authentication.www.BasicAuthenticationFilter;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;

/**
 * Spring Security configuration of the TrackMyTools eShop API.
 * 
 * Swagger UI and Spring Actuator endpoints are secured via Basic Auth with credentials being read from the eShop
 * <i>ApiConfiguration</i>.
 * 
 * All contract management endoints are secured via a <i>X-Auth-Token</i> HTTP header. Then token can be obtained via
 * the "/account/manage" API call. Unauthorized calls will be answered with a HTTP status code 401 Unauthorized.
 * 
 * 
 * As of Spring Security 4.0, @EnableWebMvcSecurity is deprecated. The replacement is @EnableWebSecurity and we include
 * it here to have to the @AuthenticationPrincipal annotation enabled.
 * 
 * @author rot8le
 *
 */
@EnableWebSecurity
@EnableGlobalMethodSecurity(prePostEnabled = true)
public class SecurityConfig {

    public SecurityConfig() {
    }

    @Configuration
    @Order(1)
    public static class ActuatorWebSecurityConfigurationAdapter extends WebSecurityConfigurerAdapter {

        @Autowired
        ApiConfiguration apiConfig;

        @Override
        protected void configure(HttpSecurity http) throws Exception {
            http.csrf().disable().sessionManagement().sessionCreationPolicy(SessionCreationPolicy.STATELESS).and()
                    .requestMatchers().antMatchers(securedEndpoints()).and().authorizeRequests().antMatchers("/**")
                    .hasRole("ADMIN").and().httpBasic().and().exceptionHandling()
                    .authenticationEntryPoint(basicAuthEntryPoint());
        }

        @Override
        protected void configure(AuthenticationManagerBuilder auth) throws Exception {
            auth.inMemoryAuthentication().withUser(apiConfig.getSecurity().getBackendAdminUsername())
                    .password(apiConfig.getSecurity().getBackendAdminPassword()).roles("USER", "ADMIN");
        }

        @Bean
        public BasicAuthenticationEntryPoint basicAuthEntryPoint() {
            final BasicAuthenticationEntryPoint basicAuthenticationEntryPoint = new BasicAuthenticationEntryPoint();
            basicAuthenticationEntryPoint.setRealmName("RB.PT.TmT.IoT Shop API");
            return basicAuthenticationEntryPoint;
        }

        private String[] securedEndpoints() {
            return new String[] { ApiController.AUTOCONFIG_ENDPOINT, ApiController.BEANS_ENDPOINT,
                    ApiController.CONFIGPROPS_ENDPOINT, ApiController.ENV_ENDPOINT, ApiController.MAPPINGS_ENDPOINT,
                    ApiController.METRICS_ENDPOINT, ApiController.TRACE_ENDPOINT, ApiController.DUMP_ENDPOINT,
                    ApiController.SHUTDOWN_ENDPOINT, SwaggerConfig.SWAGGER_UI_ENDPOINT,
                    SwaggerConfig.SWAGGER_RESOURCES_ENDPOINT, SwaggerConfig.SWAGGER_API_DOCS_ENDPOINT };
        }
    }

    @Configuration
    public static class AccountManagementWebSecurityConfigurationAdapter extends WebSecurityConfigurerAdapter {

        @Override
        protected void configure(HttpSecurity http) throws Exception {
            http.csrf().disable().sessionManagement().sessionCreationPolicy(SessionCreationPolicy.STATELESS).and()
                    .requestMatchers().antMatchers(ApiController.API_ACCOUNT_PATH + "/**").and().authorizeRequests()
                    .antMatchers(ApiController.AUTHENTICATE_URL+"/**").permitAll().and().authorizeRequests()
                    .antMatchers(HttpMethod.OPTIONS, ApiController.API_ACCOUNT_PATH + "/**").permitAll().and()
                    .authorizeRequests().anyRequest().authenticated().and().exceptionHandling()
                    .authenticationEntryPoint(unauthorizedEntryPoint());

            http.addFilterBefore(new AuthenticationFilter(authenticationManager()), BasicAuthenticationFilter.class);
        }


        @Bean
        public AuthenticationEntryPoint unauthorizedEntryPoint() {
            return new AuthenticationEntryPoint() {
                @Override
                public void commence(HttpServletRequest request, HttpServletResponse response,
                                     AuthenticationException authException) throws IOException, ServletException {

                    response.sendError(HttpServletResponse.SC_UNAUTHORIZED);
                }
            };
        }

    }

}
