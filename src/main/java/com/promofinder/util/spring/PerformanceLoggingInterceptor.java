package com.promofinder.util.spring;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.http.HttpRequest;
import org.springframework.http.client.ClientHttpRequestExecution;
import org.springframework.http.client.ClientHttpRequestInterceptor;
import org.springframework.http.client.ClientHttpResponse;
import org.springframework.util.StopWatch;

import java.io.IOException;

/**
 * Created by KAI8WZ on 04.01.2018.
 */
public class PerformanceLoggingInterceptor implements ClientHttpRequestInterceptor {

    private static final Logger LOG = LoggerFactory.getLogger(PerformanceLoggingInterceptor.class);

    @Override
    public ClientHttpResponse intercept(HttpRequest request, byte[] body, ClientHttpRequestExecution execution)
            throws IOException {

        final StopWatch stopWatch = new StopWatch(String.format("%s %s", request.getMethod(), request.getURI()));
        stopWatch.start();

        try {
            return execution.execute(request, body);
        } finally {
            stopWatch.stop();
            LOG.trace(stopWatch.shortSummary());
        }
    }

}
