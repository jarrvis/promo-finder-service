package com.promofinder.util.spring;

import com.google.common.collect.ImmutableList;
import com.promofinder.ApiConfiguration;
import org.apache.http.HttpHost;
import org.apache.http.auth.AuthScope;
import org.apache.http.auth.Credentials;
import org.apache.http.auth.UsernamePasswordCredentials;
import org.apache.http.client.CredentialsProvider;
import org.apache.http.impl.client.BasicCredentialsProvider;
import org.apache.http.impl.client.HttpClientBuilder;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.http.client.ClientHttpRequestFactory;
import org.springframework.http.client.ClientHttpRequestInterceptor;
import org.springframework.http.client.HttpComponentsClientHttpRequestFactory;
import org.springframework.http.client.InterceptingClientHttpRequestFactory;
import org.springframework.web.client.RestTemplate;

import java.util.List;

/**
 * Created by KAI8WZ on 04.01.2018.
 */
@Configuration
public class RestTemplateConfig {

    private static final Logger LOG = LoggerFactory.getLogger(RestTemplateConfig.class);

    @Autowired
    ApiConfiguration apiConfiguration;

    @Bean
    public RestTemplate restTemplate() {
        return new RestTemplate();
    }

    @Bean
    @Qualifier("PyGeoLocator")
    public RestTemplate restTemplateForPyGeoLocator() {
        final List<ClientHttpRequestInterceptor> interceptors = ImmutableList.<ClientHttpRequestInterceptor> of(
                new ApiKeyInterceptor(apiConfiguration.getPyGeoLocator().getApiKey()),
                new PerformanceLoggingInterceptor());

        final ClientHttpRequestFactory factory = new InterceptingClientHttpRequestFactory(
                new HttpComponentsClientHttpRequestFactory(), interceptors);

        final RestTemplate restTemplate = new RestTemplate(factory);

        return restTemplate;
    }

    private ClientHttpRequestFactory proxyClientHttpRequestFactory() {
        final HttpComponentsClientHttpRequestFactory factory = new HttpComponentsClientHttpRequestFactory();

        final ApiConfiguration.HttpProxy httpProxy = apiConfiguration.getHttpProxy();

        if (httpProxy == null) {
            return factory;
        }

        if (httpProxy.getHost() != null && httpProxy.getPort() != null) {
            LOG.info("Configuring proxy {}:{}", httpProxy.getHost(), httpProxy.getPort());

            int portNr = -1;
            try {
                portNr = Integer.parseInt(httpProxy.getPort());
            } catch (final NumberFormatException e) {
                LOG.warn("Unable to parse the proxy port number");
            }

            final HttpHost proxy = new HttpHost(httpProxy.getHost(), portNr);

            HttpClientBuilder builder = HttpClientBuilder.create().setProxy(proxy);

            if (httpProxy.getUser() != null && httpProxy.getPassword() != null) {
                LOG.info("Using proxy authentication");
                final Credentials credentials = new UsernamePasswordCredentials(httpProxy.getUser(),
                        httpProxy.getPassword());
                final AuthScope authScope = new AuthScope(httpProxy.getHost(), portNr);
                final CredentialsProvider credsProvider = new BasicCredentialsProvider();
                credsProvider.setCredentials(authScope, credentials);

                builder = builder.setDefaultCredentialsProvider(credsProvider);
            }

            factory.setHttpClient(builder.build());
        } else {
            LOG.info("Using no proxy");
        }

        return factory;
    }

}
