package com.promofinder;

import org.springframework.boot.context.properties.EnableConfigurationProperties;
import org.springframework.stereotype.Component;
import org.hibernate.validator.constraints.NotEmpty;
import org.springframework.boot.context.properties.ConfigurationProperties;
import org.springframework.context.annotation.Bean;

import javax.validation.Valid;
import javax.validation.constraints.NotNull;
import java.util.ArrayList;
import java.util.List;

import com.promofinder.api.filter.CorsConfiguration;

/**
 * Main configuration class for the PromoFinder API.
 * 
 * Maps all Spring configuration properties prefixed with "api" to this class and its nested subclasses. JSR 303 bean
 * validation annotations are used to ensure reasonable values are injected.
 * 
 * @author kai8wz
 *
 */
@Component
@EnableConfigurationProperties
@ConfigurationProperties(prefix = "api")
public class ApiConfiguration {

    @NotEmpty
    private final List<String> allowedLocales = new ArrayList<>();

    @NotNull
    private Security security;

    private Boolean includeStackTraceOnErrorPage = Boolean.FALSE;

    @NotNull
    private Integer sessionLifetime;

    @Valid
    @NotNull
    private Cors cors;

    @Valid
    @NotNull
    private PyGeoLocator pyGeoLocator;

    private HttpProxy httpProxy;

    /**
     * Return the list of locale string allowed to access the API.
     * 
     * @return list of locale strings
     */
    public List<String> getAllowedLocales() {
        return allowedLocales;
    }

    public Boolean getIncludeStackTraceOnErrorPage() {
        return includeStackTraceOnErrorPage;
    }

    /**
     * Set the boolean flag indicating if a stacktrace should be included on an error page.
     * 
     * @param includeStackTraceOnErrorPage
     *            boolean value
     */
    public void setIncludeStackTraceOnErrorPage(Boolean includeStackTraceOnErrorPage) {
        this.includeStackTraceOnErrorPage = includeStackTraceOnErrorPage;
    }

    /**
     * Return the lifetime of a session (in minutes).
     * 
     * @return session lifetime
     */
    public Integer getSessionLifetime() {
        return sessionLifetime;
    }

    /**
     * Set the lifetime of a session (in minutes).
     * 
     * @param sessionLifetime
     *            session lifetime
     */
    public void setSessionLifetime(Integer sessionLifetime) {
        this.sessionLifetime = sessionLifetime;
    }

    /**
     * Return the configuration for CORS requests.
     * 
     * @return Cors request configuration
     */
    public Cors getCors() {
        return cors;
    }

    /**
     * Set the configuration for CORS requests.
     * 
     * @param cors
     *            Cors request configuration
     */
    public void setCors(Cors cors) {
        this.cors = cors;
    }

    /**
     * Return configuration for a HTTP proxy (used by REST template).
     * 
     * @return proxy configuration
     */
    public HttpProxy getHttpProxy() {
        return httpProxy;
    }

    /**
     * Set the configuration for a HTTP proxy (used by REST template).
     * 
     * @param httpProxy
     *            proxy configuration
     */
    public void setHttpProxy(HttpProxy httpProxy) {
        this.httpProxy = httpProxy;
    }

    /**
     * Return the configuration for the PyGeoLocator webservice.
     *
     * @return configuration for PyGeoLocator webservice
     */
    public PyGeoLocator getPyGeoLocator() {
        return pyGeoLocator;
    }

    /**
     * Set the configuration for the PyGeoLocator webservice.
     *
     * @param pyGeoLocator
     *            configuration for PyGeoLocator webservice
     */
    public void setPyGeoLocator(PyGeoLocator pyGeoLocator) {
        this.pyGeoLocator = pyGeoLocator;
    }

    /**
     * Nested configuration subclass to encapsulate all config values for the eShop web application.
     * 
     * @author rot8le
     *
     */
    public static class Shop {
        private String scheme = "http";

        @NotEmpty
        private String host;

        private Integer port;

        @NotEmpty
        private String purchaseSummaryPath;

        @NotEmpty
        private String accountManagementPath;

        /**
         * Return the HTTP scheme for the web URL of the eShop website.
         * 
         * @return HTTP scheme string
         */
        public String getScheme() {
            return scheme;
        }

        /**
         * Set the HTTP scheme for the web URL of the eShop website.
         * 
         * @param scheme
         *            HTTP scheme string
         */
        public void setScheme(String scheme) {
            this.scheme = scheme;
        }

        /**
         * Return the host name for the web URL of the eShop website.
         * 
         * @return host name string
         */
        public String getHost() {
            return host;
        }

        /**
         * Set the host name for the web URL of the eShop website.
         * 
         * @param host
         *            host name string
         */
        public void setHost(String host) {
            this.host = host;
        }

        /**
         * Return the port for the web URL of the eShop website.
         * 
         * @return int value
         */
        public Integer getPort() {
            return port;
        }

        /**
         * Set the port for the web URL of the eShop website.
         * 
         * @param port
         *            int value
         */
        public void setPort(Integer port) {
            this.port = port;
        }

        /**
         * Return the path for the purchase summary overview page in the eShop website.
         * 
         * @return path to the purchase summary page
         */
        public String getPurchaseSummaryPath() {
            return purchaseSummaryPath;
        }

        /**
         * Set the path for the purchase summary page in the eShop website.
         * 
         * @param purchaseSummaryPath
         *            path to purchase summary
         */
        public void setPurchaseSummaryPath(String purchaseSummaryPath) {
            this.purchaseSummaryPath = purchaseSummaryPath;
        }

        /**
         * Return the path for account management in the eShop website.
         * 
         * @return account management path
         */
        public String getAccountManagementPath() {
            return accountManagementPath;
        }

        /**
         * Set the path for account management in the eShop website.
         * 
         * @param accountManagementPath
         *            path to account management
         */
        public void setAccountManagementPath(String accountManagementPath) {
            this.accountManagementPath = accountManagementPath;
        }
    }

    /**
     * Nested configuration subclass to encapsulate all supported config values to setup CORS support in the eShop API.
     * 
     * @author rot8le
     *
     */
    public static class Cors {
        private List<String> allowedOrigins;

        private List<String> allowedMethods;

        private List<String> allowedHeaders;

        private List<String> exposedHeaders;

        private Long maxAge;

        public List<String> getAllowedOrigins() {
            return allowedOrigins;
        }

        public void setAllowedOrigins(List<String> allowedOrigins) {
            this.allowedOrigins = allowedOrigins;
        }

        public List<String> getAllowedMethods() {
            return allowedMethods;
        }

        public void setAllowedMethods(List<String> allowedMethods) {
            this.allowedMethods = allowedMethods;
        }

        public List<String> getAllowedHeaders() {
            return allowedHeaders;
        }

        public void setAllowedHeaders(List<String> allowedHeaders) {
            this.allowedHeaders = allowedHeaders;
        }

        public List<String> getExposedHeaders() {
            return exposedHeaders;
        }

        public void setExposedHeaders(List<String> exposedHeaders) {
            this.exposedHeaders = exposedHeaders;
        }

        public Long getMaxAge() {
            return maxAge;
        }

        public void setMaxAge(Long maxAge) {
            this.maxAge = maxAge;
        }
    }

    /**
     * Returns and the CORS configuration of the eShop API as a Spring bean.
     * 
     * @return CorsConfiguration bean
     */
    @Bean
    public CorsConfiguration corsConfiguration() {
        final CorsConfiguration corsConfiguration = new CorsConfiguration();
        if (getCors().getAllowedOrigins() != null && !getCors().getAllowedOrigins().isEmpty()) {
            corsConfiguration.setAllowedOrigins(getCors().getAllowedOrigins());
        }
        if (getCors().getAllowedMethods() != null && !getCors().getAllowedMethods().isEmpty()) {
            corsConfiguration.setAllowedMethods(getCors().getAllowedMethods());
        }
        if (getCors().getAllowedHeaders() != null && !getCors().getAllowedHeaders().isEmpty()) {
            corsConfiguration.setAllowedHeaders(getCors().getAllowedHeaders());
        }
        if (getCors().getExposedHeaders() != null && !getCors().getExposedHeaders().isEmpty()) {
            corsConfiguration.setExposedHeaders(getCors().getExposedHeaders());
        }
        corsConfiguration.setMaxAge(getCors().getMaxAge());
        return corsConfiguration;
    }


    /**
     * Nested configuration subclass to encapsulate all config values to cofigure a proxy connection.
     * 
     * @author rot8le
     *
     */
    public static class HttpProxy {
        private String host;
        private String port;
        private String user;
        private String password;

        /**
         * Return the host name of the proxy server.
         * 
         * @return proxy host
         */
        public String getHost() {
            return host;
        }

        /**
         * Set the host name of the proxy server.
         * 
         * @param host
         *            host name
         */
        public void setHost(String host) {
            this.host = host;
        }

        /**
         * Return the port of the proxy server.
         * 
         * @return proxy port number string
         */
        public String getPort() {
            return port;
        }

        /**
         * Set the port of the proxy server.
         * 
         * @param port
         *            port number string
         */
        public void setPort(String port) {
            this.port = port;
        }

        /**
         * Return the username used to authenticate against the proxy server.
         * 
         * @return proxy user name
         */
        public String getUser() {
            return user;
        }

        /**
         * Set the username used to authenticate against the proxy server.
         * 
         * @param user
         *            user name
         */
        public void setUser(String user) {
            this.user = user;
        }

        /**
         * Return the password used to authenticate against the proxy server.
         * 
         * @return proxy user password
         */
        public String getPassword() {
            return password;
        }

        /**
         * Set the password used to authenticate against the proxy server.
         * 
         * @param password
         *            proxy user password
         */
        public void setPassword(String password) {
            this.password = password;
        }
    }

    public static class Security {

        @NotEmpty
        private String apikeyHeaderName;
        @NotEmpty
        private List<ApiKey> authenticatedApikeys;

        private String backendAdminUsername;

        private String backendAdminPassword;

        private Boolean deleteSecurityToken = Boolean.TRUE;

        public String getApikeyHeaderName() {
            return apikeyHeaderName;
        }

        public void setApikeyHeaderName(String apikeyHeaderName) {
            this.apikeyHeaderName = apikeyHeaderName;
        }

        public List<ApiKey> getAuthenticatedApikeys() {
            return authenticatedApikeys;
        }

        public void setAuthenticatedApikeys(List<ApiKey> authenticatedApikeys) {
            this.authenticatedApikeys = authenticatedApikeys;
        }

        /**
         * Return the username used to secure the API's Swagger and Spring
         * Actuator endpoints.
         *
         * @return user name string
         */
        public String getBackendAdminUsername() {
            return backendAdminUsername;
        }

        /**
         * Set the username used to secure the API's Swagger and Spring Actuator
         * endpoints.
         *
         * @param backendAdminUsername
         *            user name string
         */
        public void setBackendAdminUsername(String backendAdminUsername) {
            this.backendAdminUsername = backendAdminUsername;
        }

        /**
         * Return the password used to secure the API's Swagger and Spring
         * Actuator endpoints.
         *
         * @return password string
         */
        public String getBackendAdminPassword() {
            return backendAdminPassword;
        }

        /**
         * Set the password used to secure the API's Swagger and Spring Actuator
         * endpoints.
         *
         * @param backendAdminPassword
         *            password string
         */
        public void setBackendAdminPassword(String backendAdminPassword) {
            this.backendAdminPassword = backendAdminPassword;
        }

        /**
         * Return the boolean flag indicating if the security token in the subscription object should be deleted upon
         * creation of an account management session.
         *
         * @return boolean value
         */
        public Boolean getDeleteSecurityToken() {
            return deleteSecurityToken;
        }

        /**
         * Set the boolean flag indicating if the security token in the subscription object should be deleted upon creation
         * of an account management session.
         *
         * @param deleteSecurityToken
         *            boolean value
         */
        public void setDeleteSecurityToken(Boolean deleteSecurityToken) {
            this.deleteSecurityToken = deleteSecurityToken;
        }

    }

    public static class ApiKey {
        @NotEmpty
        private String name;
        @NotEmpty
        private String apikey;
        @NotEmpty
        private String purpose;

        public String getName() {
            return name;
        }

        public void setName(String name) {
            this.name = name;
        }

        public String getApikey() {
            return apikey;
        }

        public void setApikey(String apikey) {
            this.apikey = apikey;
        }

        public String getPurpose() {
            return purpose;
        }

        public void setPurpose(String purpose) {
            this.purpose = purpose;
        }

    }

    /**
     * Nested configuration subclass to encapsulate all config values to setup the REST interface to the PyGeoLocator
     * webservice API.
     *
     * @author kai8wz
     *
     */
    public static class PyGeoLocator {
        @NotEmpty
        private String url;

        @NotEmpty
        private String apiKey;

        /**
         * Return the URL of the PyGeoLocator webservice.
         *
         * @return url
         */
        public String getUrl() {
            return url;
        }

        /**
         * Set the URL of the PyGeoLocator webservice.
         *
         * @param url
         *            url
         */
        public void setUrl(String url) {
            this.url = url;
        }

        /**
         * Get the API key used to authenticate against the PyGeoLocator webservice.
         *
         * @return API key string
         */
        public String getApiKey() {
            return apiKey;
        }

        /**
         * Set the API key used to authenticate against the PyGeoLocator webservice.
         *
         * @param apiKey
         *            API key string
         */
        public void setApiKey(String apiKey) {
            this.apiKey = apiKey;
        }
    }

    public Security getSecurity() {
        return security;
    }

    public void setSecurity(Security security) {
        this.security = security;
    }
}
