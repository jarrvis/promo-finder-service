package com.promofinder.service.repository;

import com.promofinder.model.LocationData;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.jpa.repository.JpaRepository;

import java.util.List;

/**
 * Created by KAI8WZ on 01.10.2017.
 */
public interface LocationDataRepository extends JpaRepository<LocationData, String> {
    LocationData findByName(String name);

    List<LocationData> findAll();

    /**
     * Retrieve a list of locations.
     *
     * @param sortAndLimit
     *            sorting and limiting results
     * @return List of Location objects or null if not found
     */
    Page<LocationData> findAll(Pageable sortAndLimit);

}
