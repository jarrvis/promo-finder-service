package com.promofinder.service.repository;

import com.promofinder.model.Product;
import org.springframework.data.jpa.repository.JpaRepository;

/**
 * Created by KAI8WZ on 13.06.2017.
 */

public interface ProductRepository extends JpaRepository<Product, String> {

    public Product findByName(String name);

}
