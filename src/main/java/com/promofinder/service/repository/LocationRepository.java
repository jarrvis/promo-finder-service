package com.promofinder.service.repository;

/**
 * Created by KAI8WZ on 11.06.2017.
 */

import com.promofinder.model.Location;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;

import java.util.List;


public interface LocationRepository extends JpaRepository<Location, String> {

    String FIND_CLOSEST = "SELECT id,name,longitude,latitude,address1,address2,address3, SQRT(\n" +
            "    POW(111 * (latitude - ?1), 2) +\n" +
            "    POW(111 * (?2 - longitude) * COS(latitude / 57.3), 2)) AS distance\n" +
            "FROM location HAVING distance < ?3 ORDER BY distance LIMIT ?4";

    String FIND_CLOSEST_WITH_PAGING = "SELECT id,name,longitude,latitude,address1,address2,address3, SQRT(\n" +
            "    POW(111 * (latitude - ?1), 2) +\n" +
            "    POW(111 * (?2 - longitude) * COS(latitude / 57.3), 2)) AS distance\n" +
            "FROM location HAVING distance < ?3 ORDER BY distance LIMIT ?4";

    /**
     * Retrieve a list of locations identified by the subscriptionId.
     *
     * @param latitude
     *            latitude of source requesting closest locations
     * @param longitude
     *            longitude of source requesting closest locations
     * @param areaRadius
     *            radius of area in which searching should be done
     * @param howMany
     *            maximum number of locations that will be fetched
     * @return List of Location objects or null if not found
     */
    @Query(nativeQuery=true, value=FIND_CLOSEST)
    List<Location> findClosest(double latitude, double longitude, double areaRadius, int howMany);

   // List<Location> findClosest(double latitude, double longitude, double areaRadius, int howMany);

    /**
     * Retrieve a list of locations identified by the subscriptionId.
     *
     * @param name
     *            name of location
     * @return List of Location objects or null if not found
     */
    List<Location> findByName(String name);

    /**
     * Retrieve a list of locations.
     *
     * @param sortAndLimit
     *            sorting and limiting results
     * @return List of Location objects or null if not found
     */
    Page<Location> findAll(Pageable sortAndLimit);


}
