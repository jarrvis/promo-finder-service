package com.promofinder.service.repository;

import com.promofinder.model.Location;
import com.promofinder.model.LocationData;

import java.util.List;
import java.util.Optional;

/**
 * Created by KAI8WZ on 04.10.2017.
 */
public interface FeedRepository {

    Optional<List<Location>> readFeedFile(String feedFileSource, String fileName);
    Optional<List<LocationData>> readFeedFiles(String feedSource);
    List<String> findFeedFiles(String feedSource);
}
