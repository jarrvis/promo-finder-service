package com.promofinder.service.remote;

import java.util.List;

/**
 * Created by KAI8WZ on 04.01.2018.
 */
public interface PyGeoLocator {

    /**
     * Generate feed file with locations based on google query name and place type for Warsaw.
     *
     * @param name
     *            name identifying the location
     *
     * @param type
     *            name identifying the location
     */
    void generateFeed(String name, String type);

    /**
     * Generate feed file with locations based on google query name and place type for given square.
     *
     * @param name
     *            name identifying the location
     *
     * @param type
     *            name identifying the location
     *
     *  @param startLongitude
     *            longitude of left upper corner of square to search
     *
     * @param startLatitude
     *            latitude of left upper corner of square to search
     *
     * @param endLongitude
     *            longitude of right lower corner of square to search
     *
     *@param endLatitude
     *            latitude of right lower corner of square to search
     */
    void generateFeed(String name, String type, String startLongitude, String startLatitude, String endLongitude, String endLatitude);

    /**
     * Get feed file names ready for import .
     */
    List<String> getFeedFileNames();

    /**
     * Download all feed files ready for import .
     */
    void getAllFeedFile();

    /**
     * Download feed file ready for import .
     *
     * @param name
     *            name identifying the feed file name
     */
    void getFeedFile(String name);
}
