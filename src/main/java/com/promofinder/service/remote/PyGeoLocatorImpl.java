package com.promofinder.service.remote;

import com.promofinder.ApiConfiguration;
import com.promofinder.api.exception.ServiceUnavailableException;
import org.apache.commons.io.FileUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.converter.ByteArrayHttpMessageConverter;
import org.springframework.stereotype.Service;
import org.springframework.web.client.HttpStatusCodeException;
import org.springframework.web.client.RestClientException;
import org.springframework.web.client.RestTemplate;
import org.springframework.web.util.UriComponentsBuilder;

import java.io.File;
import java.io.IOException;
import java.util.List;

/**
 * Created by KAI8WZ on 04.01.2018.
 */
@Service
public class PyGeoLocatorImpl implements PyGeoLocator {

    private static final Logger LOG = LoggerFactory.getLogger(PyGeoLocatorImpl.class);

    @Autowired
    ApiConfiguration apiConfiguration;

    @Autowired
    //@Qualifier("PyGeoLocator")
    protected RestTemplate restTemplate;

    @Override
    public void generateFeed(String name, String type) {

    }

    @Override
    public void generateFeed(String name, String type, String startLongitude, String startLatitude, String endLongitude, String endLatitude) {

    }

    @Override
    public List<String> getFeedFileNames() {
        return null;
    }

    @Override
    public void getAllFeedFile() {

        final String uri = UriComponentsBuilder.fromHttpUrl(apiConfiguration.getPyGeoLocator().getUrl())
                .path("/get-all").build().toUriString();

        LOG.debug("Fetching product data from webservice at {}", uri);

        try {
            restTemplate.getMessageConverters().add(
                    new ByteArrayHttpMessageConverter());

            byte[] feedBytes = restTemplate.getForObject(uri,  byte[].class);

            FileUtils.writeByteArrayToFile(new File("FEED"), feedBytes);

        } catch (final HttpStatusCodeException e) {
            if (e.getStatusCode() == HttpStatus.NOT_FOUND) {
                return;
            }

            LOG.error("Error fetching feed from remote PyGeoLocator service, cause: {}",
                    e.getResponseBodyAsString(), e);
            throw e;
        } catch (final RestClientException | IOException e) {
            LOG.error("Error fetching feed from remote PyGeoLocator service", e);
            throw new ServiceUnavailableException(e.getMessage());
        }
    }

    @Override
    public void getFeedFile(String name) {

    }
}
