package com.promofinder.service;

import com.promofinder.service.repository.FeedRepository;
import com.promofinder.api.exception.NotFoundException;
import com.promofinder.api.exception.OperationNotAllowedException;
import com.promofinder.model.Location;
import com.promofinder.model.LocationData;
import org.apache.commons.io.FilenameUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.core.env.Environment;
import org.springframework.stereotype.Service;

import javax.transaction.Transactional;
import java.io.*;
import java.util.*;

/**
 * Created by KAI8WZ on 04.10.2017.
 */
@Service
@Transactional
public class FeedService implements FeedRepository{

    private static final Logger LOG = LoggerFactory.getLogger(FeedService.class);
    private static final String FEED_FILE_LABELS = "promofinder.data.feed.labels";

    @Autowired
    private Environment env;

    @Override
    public Optional<List<Location>> readFeedFile(String feedSource, String fileName) {
        String feedFilePath = feedSource + fileName + ".log";
        try(FileInputStream fis = new FileInputStream(new File(feedFilePath))){
            BufferedReader br = new BufferedReader(new InputStreamReader(fis, "UTF-8"));
            String line;
            if((line = br.readLine()) == null)
                return null;
            final List<String> fields = Arrays.asList(line.split(", "));
            HashMap<String, Integer> labelsMap = new HashMap<>() ;
            List<String> feedLabels = Arrays.asList(env.getProperty(FEED_FILE_LABELS).split(","));
            feedLabels.forEach(label -> {
                Optional<String> value = fields
                        .stream()
                        .filter(field -> field.equals(label))
                        .findFirst();
                value.ifPresent(x ->
                        labelsMap.put(label,fields.indexOf(x))
                );
            });

            List<Location> locations = new ArrayList<>();
            HashMap<String,String> attributes = new HashMap<>();
            while ((line = br.readLine()) != null) {
                List<String> locationAttributes = Arrays.asList(line.split(", "));
                labelsMap.keySet()
                        .forEach(key -> {
                            if (labelsMap.get(key) < locationAttributes.size())
                                attributes.put(key, locationAttributes.get(labelsMap.get(key)));
                        });
                locations.add(new Location(attributes));
            }
            br.close();
            return Optional.of(locations);
        }
    catch (Exception e){
            throw new OperationNotAllowedException("Cannot read source data file from feed directory");
    }
    }

    @Override
    public Optional<List<LocationData>> readFeedFiles(String feedSource) {
        List<String> fileNames = this.findFeedFiles(feedSource);
        if(fileNames.isEmpty())
            throw new NotFoundException("Not found any feed files");

        List<LocationData> locationDataList  = new ArrayList<>();
        fileNames.forEach(name ->
                this.readFeedFile(feedSource, name)
                        .ifPresent(list ->
                                locationDataList.add(new LocationData(list, name))));

        return Optional.of(locationDataList);
    }

    @Override
    public List<String> findFeedFiles(String feedSource) {

        Optional<File[]> feedFiles = Optional.ofNullable(new File(feedSource).listFiles());
        if (!feedFiles.isPresent())
            throw new NotFoundException("No results found");

        List<String> feedFileNames = new ArrayList<>();
        Arrays.asList(feedFiles.get()).forEach(file -> feedFileNames.add(FilenameUtils.removeExtension(file.getName())));
        return feedFileNames;
    }
}

