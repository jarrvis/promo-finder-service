package com.promofinder.service;

import com.promofinder.api.exception.NotFoundException;
import com.promofinder.model.Location;
import com.promofinder.model.LocationData;
import com.promofinder.service.repository.LocationDataRepository;
import com.promofinder.service.repository.LocationRepository;
import io.vavr.Tuple;
import io.vavr.Tuple4;

import io.vavr.control.Try;
import org.apache.commons.lang3.StringUtils;
import org.apache.commons.lang3.math.NumberUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.domain.Pageable;
import org.springframework.stereotype.Service;

import javax.transaction.Transactional;
import java.util.List;
import java.util.Optional;

/**
 * Created by KAI8WZ on 25.09.2017.
 */


@Service
@Transactional
public class LocationService {

    private static final Logger LOG = LoggerFactory.getLogger(LocationService.class);

    @Autowired
    LocationRepository locationRepository;

    @Autowired
    LocationDataRepository locationDataRepository;

    public List<Location> findClosest(String latitude, String longitude, String areaRadius, String howMany, PageRequest pageRequest){

        Tuple4<Double, Double, Double, Integer> result = getValidatedParams(latitude, longitude, areaRadius, howMany).get();
       // return locationRepository.findClosest(result._1,  result._2, result._3, result._4, pageRequest);
        return null;
    }
    public List<Location> findClosest(String latitude, String longitude, String areaRadius, String howMany){

        Tuple4<Double, Double, Double, Integer> result = getValidatedParams(latitude, longitude, areaRadius, howMany).get();
        return locationRepository.findClosest(result._1,  result._2, result._3, result._4);
    }

    private Try<Tuple4<Double, Double, Double, Integer>> getValidatedParams(String latitude, String longitude, String areaRadius, String howMany) {
        Try<Tuple4<Double,Double,Double,Integer>> tuple = Try.of(() ->
                Tuple.of(
                NumberUtils.createDouble(latitude),
                NumberUtils.createDouble(longitude),
                NumberUtils.createDouble(areaRadius),
                NumberUtils.createInteger(howMany)
                ));

        io.vavr.collection.List lst = io.vavr.collection.List.of(1,2,3);
        lst.asJava();

        tuple.onFailure(t ->{
            LOG.info("Could not fetch closest locations for location: latitude={}, longitude={}", latitude, longitude);
            throw new NotFoundException("Could not fetch closest locations for location");
        });
        return tuple;
    }

    public LocationData findByName(String name){
        Optional<LocationData> locations = Optional.ofNullable((StringUtils.isEmpty(name) ?
                null : locationDataRepository.findByName(name)));
        if(!locations.isPresent())
            throw new NotFoundException("Location object could not be found in DB");
        return locations.get();
    }

    public List<LocationData> findAll(Pageable orderAndSort){
        Optional<Page<LocationData>> locations = Optional.ofNullable(locationDataRepository.findAll(orderAndSort));
        if(!locations.isPresent())
            throw new NotFoundException("Location object could not be found in DB");
        return locations.get().getContent();
    }

    public List<LocationData> findAll(){
        Optional<List<LocationData>> locations = Optional.ofNullable(locationDataRepository.findAll());
        if(!locations.isPresent())
            throw new NotFoundException("Location object could not be found in DB");
        return locations.get();
    }

    public Location delete(String id){
        Optional<Location> location = Optional.ofNullable(locationRepository.findOne(id));
        if(!location.isPresent())
            throw new NotFoundException("Location object could not be found in DB");
        locationRepository.delete(id);
        return location.get();
    }

    public void saveOrUpdate(LocationData locationData){
        Optional.ofNullable(this.findByName(locationData.getName())).ifPresent(data -> {
            this.locationDataRepository.delete(data);
        } );
        locationDataRepository.save(locationData);
    }

    public void saveAll(List<LocationData> locationDataList){
        locationDataList.forEach(locationData -> {
            Optional.ofNullable(locationDataRepository.findByName(locationData.getName())).ifPresent(data -> {
                this.locationDataRepository.delete(data);
            } );
        });
        locationDataRepository.save(locationDataList);
    }

    public void updateWithImageUrl(String name, String imageUrl){
        LocationData locationData = this.findByName(name);
        locationData.setImageUrl(imageUrl);
        this.saveOrUpdate(locationData);
    }
}
